#!/usr/bin/env bash
phy=$1
[ -z "$phy" ] && phy='phy0'
interfaces="$(iw dev | grep Interface | awk '{print $2}')"
monpresent=""
if [ -n "$interfaces" ]; then
	for iface in $interfaces; do
	#	if [ "$iface" = "mon0" ]; then
	#		echo "[info] Monitor interface mon0 already present"
	#		monpresent="yes"
	#	else
			echo "[info] Interface $iface detected - deleting"
			echo "> iw $iface del"
			iw $iface del || exit 1
	#	fi
	done
fi
if [ "$monpresent" != "yes" ]; then
	echo "[info] Creating monitor interface"
	echo "> iw $phy interface add mon0 type monitor flags fcsfail"
	iw $phy interface add mon0 type monitor flags fcsfail || exit 1
fi
echo "[info] bringing the interface up"
echo "> ip link set mon0 up"
ip link set mon0 up
echo "> iw dev"
iw dev
