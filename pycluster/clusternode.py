from .host import Host

class ClusterNode(Host):
    "Host with added WLAN interface"
    
    def __init__(self, hostname, ip_lan, ip_wlan, *args, **kwargs):
        super().__init__(hostname, ip_lan, *args, **kwargs)
        self.ip_wlan = ip_wlan
        self.cluster = None # is set when added to a Cluster object

    # inherit
    #def __repr__(self):
        
    def __str__(self):
        return "<ClusterNode {}, {} (lan), {} (wifi)>".format(self.hostname, self.ip, self.ip_wlan)