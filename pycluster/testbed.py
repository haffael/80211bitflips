import time
import os
import traceback

from datetime import datetime

from .cluster import Cluster
from .clusternode import ClusterNode
from .host import Host, ExitcodeNotNullException

class Testbed:
    """Represents a Group of `Cluster`s, an accesspoint (type `Host`) and possibly utility hosts (e.g. unicast destination).
    
    """

    def __init__(self, clusters, ap):
    
        self.clusters = []
        for cl in clusters:
            if isinstance(cl, Cluster):
                self.clusters.append(cl)
            else:
                raise ValueError('clusters: expected instance of Cluster, got {}'.format(type(clusters)))
        
        if isinstance(ap, Host):
            self.ap = ap
        else:
            raise ValueError('ap: expected instance of Host, got {}'.format(type(ap)))

        # self.util_hosts = dict()
        # for ut_key, ut_val in util_hosts.items():
        #     if isinstance(cl, Host):
        #         self.util_hosts[ut_key] = ut_val
        #     else:
        #         raise ValueError('util_hosts "{}": expected instance of Host, got {}'.format(ut_key, type(clusters)))


        # all clusternodes in one list - for convenience
        self.clusternodes = [ node for cluster in self.clusters for node in cluster ]
    
    def nodes_slurp(self, path, destination_local_folder, delete=True):
        "gets <path> from all clusternodes and writes to folder <destination_local_folder>. Deletes original files unless delete=False is given"
        for n in self.clusternodes:
            try:
                n.slurp(
                    path,
                    destination_local_folder,
                    delete
                )
            except Exception as e:
                print("{}: Gracefully caught Exception:".format(n))
                traceback.print_exc()

    def ap_cmd(self, cmd, print_stdout=False):
        return self.ap.cmd(cmd, print_stdout=print_stdout)
            
    def nodes_cmd(self, cmd, print_stdout=False):
        "Executes a command on all clusternodes. Blocks until finished. Returns nothing"
        for node in self.clusternodes:
            node.cmd(cmd, print_stdout=print_stdout) # raises ExitcodeNotNullException, OutputTooLongError

#     def shellcmd(self, cmd):
#         "executes a command on all nodes via the 'standard' shell"
#         for node in self.clusternodes:
#             print("sending ", cmd, "to node", node)
#             node.shell.send(cmd)
#             time.sleep(.5)
#             stdin = node.shell.makefile('wb', -1)
#             stdout = node.shell.makefile('r', -1)
#             stderr = node.shell.makefile_stderr('r', -1)
#             print('--stdout--\n', ''.join(stdout.readlines()), '\n--end--')
#             #print('--stderr--\n', ''.join(stderr.readlines()), '\n--end--')
#             if(node.shell.recv_stderr_ready()):
#                 raise RuntimeException(cmd, "failed. Stderr says:", node.shell.recv_stderr(1024))
    
    def nodes_sessioncmd(self, cmd):
        "Executes a command on all nodes. Returns a dict with the sessions and does not block"
        nodesessions = {}
        for node in self.clusternodes:
            nodesessions[node] = node.sessioncmd(cmd)
        return nodesessions
    
    @staticmethod
    def eof_sessions(sessions):
        "send EOF to the given sessions via session.shutdown_write() and return dict with exit codes"
        exitcodes = {}
        for node, nodesession in sessions.items():
            nodesession.shutdown_write()
            time.sleep(.1)
            if(nodesession.exit_status_ready()): 
                exitcodes[node] = nodesession.recv_exit_status()
            else:
                raise RuntimeError(
                    "Cmd did not exit after session.shutdown_write() and waiting 0.1s\n"
                    "node {}, session {}".format(node, nodesession)
                )
        return exitcodes
    
    @staticmethod
    def kill_sessions(sessions, print_stdout=False):
        "kill the running processes using the saved PIDs"
        for node, nodesession in sessions.items():
            # check if the process is still running
            try:
                node.cmd("ps aux | grep -v grep | grep {}".format(nodesession.cmd_pid))
            except ExitcodeNotNullException:
                print("\n{}: kill not needed, command has already terminated!".format(node))
                print("{}: command stdout:\n  {}".format(node, '  \n'.join(nodesession.makefile().readlines())))
                print("{}: command stderr:\n  {}".format(node, '  \n'.join(nodesession.makefile_stderr().readlines())))
            else:
                if print_stdout: print(node, ": killing PID {:d}".format(nodesession.cmd_pid))
                node.cmd("sudo kill {:d}".format(nodesession.cmd_pid))

    @staticmethod
    def get_stdout_from_sessions(sessions, print_stdout=False):
        stdout = dict()
        for node, nodesession in sessions.items():
            lines = []
            while nodesession.recv_ready():
                line = nodesession.recv(4096)
                if line == b'': break
                lines.append(line)
            stdout[node] = b''.join(lines).decode('utf-8')
        return stdout
            
    @staticmethod
    def get_stderr_from_sessions(sessions, print_stdout=False):
        stdout = dict()
        for node, nodesession in sessions.items():
            lines = []
            while nodesession.recv_stderr_ready():
                line = nodesession.recv_stderr(4096)
                if line == b'': break
                lines.append(line)
            stdout[node] = b''.join(lines).decode('utf-8')
        return stdout
