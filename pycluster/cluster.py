from .clusternode import ClusterNode

class Cluster(list):
    "One Cluster of ClusterNodes"

    def __init__(self, name, nodelist):
        self.name = name
        for node in nodelist:
            if not isinstance(node, ClusterNode): raise ValueError('Expected ClusterNode instance, {} instance found'.format(type(node)))
            self.append(node)
            node.cluster = self # backreference
    
    def __repr__(self):
        return "Cluster {}, {} nodes: {}".format(self.name, len(self), ", ".join([node.hostname for node in self]))

    def __str__(self):
        return self.__repr__()