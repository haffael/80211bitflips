import os
import paramiko

from scp import SCPClient
from termcolor import colored, cprint

from util import print_t, sprint_t

class ExitcodeNotNullException(Exception):
    pass

class OutputTooLongError(Exception):
    pass

class Host:

    "Simple host with hostname and LAN IP"
    def __init__(self, hostname, ip, username=None):
        self.hostname = hostname
        self.ip = ip
        self.username = username
        self.ssh_connect()
        #self.open_shell()

    def __repr__(self):
        return "<ClusterNode {}>".format(self.hostname)
        
    def __str__(self):
        return "<ClusterNode {}, {}>".format(self.hostname, self.ip)
    
    def ssh_connect(self):
        self.ssh = paramiko.SSHClient()
        self.ssh.load_system_host_keys()
        if self.username is not None:
            self.ssh.connect(self.ip, username=self.username)
        else:
            self.ssh.connect(self.ip)

    # def open_shell(self):
    #     #TODO: do we really need a shell?
    #     self.shell = self.ssh.invoke_shell()
    #     self.shell.send('\n'); self.shell.recv(1000) # flush "Last login" line

    #def check_output(self, cmd, *args, **kwargs):
    #    "similar to subprocess.check_output: runs <cmd> and returns stdout or throws CalledProcessError"
    #    stdin, stdout, stderr = self.ssh.exec_command(cmd, *args, **kwargs)
    #    e = CalledProcessError('[SSH] command failed: {}'.)
    #    e.returncode = self.ssh.

    def cmd(self, cmd, print_stdout=False):
        "Executes a command. Blocks until finished"
        session = self.ssh.get_transport().open_session()
        session.settimeout(10)
        session.exec_command(cmd)
        
        if print_stdout:
            #time.sleep(1) # give it a little time..
            prefix = colored(repr(self), 'grey', attrs=['bold'])
            print_t(prefix, cmd)
            while session.recv_ready() or not session.exit_status_ready():
                line = session.recv(4096)
                print_t(prefix, line.decode('utf8'))

        exitcode = session.recv_exit_status() # if not yet finished, blocks
        
        if exitcode != 0:
            err = []
            while session.recv_stderr_ready():
                line = session.recv_stderr(4096)
                if line == b'': break
                err.append(line)
            # Return an exception with the exitcode included
            e = ExitcodeNotNullException("Command '{cmd}' on {node} terminated with exitcode {exitcode}\n Stderr says: {err}".format(
                exitcode=exitcode, cmd=cmd, node=self, err=str(b''.join(err), 'utf8')
            ))
            e.cmd = cmd
            e.node = self
            e.exitcode = exitcode
            raise e

        max_ret_bytes = 4096
        ret_str = str(session.recv(max_ret_bytes),'utf8') # return max. 4096 byte
        if session.recv_ready(): # there is even more!
            raise OutputTooLongError("Stopped reading stdout from cmd {} after {} bytes".format(cmd, max_ret_bytes))
        
        return ret_str

    def sessioncmd(self, cmd, cd_folder='.'):
        "Executes a command and returns the session of the running command. Does not block"
        session = self.ssh.get_transport().open_session()
        #session.get_pty() # s.t. shutting down the session also sends SIGHUP to running command! https://stackoverflow.com/a/40782342
        # not working apparently.. instead, run subshell (see above)
        #session.exec_command(cmd)
        session.exec_command("bash -c 'echo $$; cd {} && exec {cmd}'".format(cd_folder, cmd=cmd.replace("'","'\''"))) # proper escaping!
        session.cmd_pid = int(str(session.recv(64), 'utf8'))
        #print("got PID:", session.cmd_pid)
        return session

    def slurp(self, remote_path, dest='./', delete=True):
        "Get a file <remote_path>, put into folder <dest>. Deletes original file(s) unless delete=False is given"
        if not self.ssh: raise Exception('No SSH connection')
        with SCPClient(self.ssh.get_transport(), sanitize=lambda x: x) as scp:
            if not os.path.isdir(dest):
                #print('had to create',dest)
                os.makedirs(dest)
            scp.get(remote_path, local_path=dest, preserve_times=True)
        if delete: self.ssh.exec_command('rm '+remote_path)

    def put(self, path, remote_dest='~/'):
        "Copy a local folder <path> to remote folder <remote_dest>"
        if not self.ssh: raise Exception('No SSH connection')
        with SCPClient(self.ssh.get_transport(), sanitize=lambda x: x) as scp:
            if not os.path.isdir(path):
                raise ValueError('{}: that directory doesnt exist.'.format(path))
            scp.put(path, remote_dest, recursive=True, preserve_times=True)
