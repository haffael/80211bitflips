#!/usr/bin/env python3
#import numpy as np
import time
import json
import socket
import os

import argparse
import subprocess

#from pprint import pprint
from datetime import datetime

from pycluster.host import Host, ExitcodeNotNullException, OutputTooLongError
from pycluster.clusternode import ClusterNode
from pycluster.cluster import Cluster
from pycluster.testbed import Testbed

from util import print_t, sprint_t, Bunch

"Update system time"
def set_date_from_ntp(): 
    subprocess.call(['sudo', 'ntpdate', 'time.zih.tu-dresden.de'])

parser = argparse.ArgumentParser(description='Boschbed wlan scan')

parser.add_argument('--outdir', action='store', type=str, default=False, help='Folder to put a wlan scan in.')
parser.add_argument('--noconfirm', action='store_const', const=True, default=False, help='Supress confirmation dialog for foldernames')
parser.add_argument('--wifistandard', '-w', type=str, choices=['11n','11g'], default='11n', help='Standard (11g or 11n). Default: 11n' )
parser.add_argument('--channel', '-ch', type=int, default=999, help='Wifi channel(s). Default: 140 (11n), 11 (11g)')
parser.add_argument('--txpower', '-t', type=int, default=5, help='Txpower(s) in dBm. Default 5')
parser.add_argument('--mcs', '-m', type=int, default=15, help='MCS(s) for 11n. Default: 15')
parser.add_argument('--ratekbps', '-r', type=int, default=6000, help='Rate in kbps for 11g. Default: 6000')
parser.add_argument('--clusters', '-c', type=str, nargs='*', default=['aa', 'bb', 'cc', 'dd'], help='Cluster(s) to capture on. Default: aa bb cc dd')
parser.add_argument('--nodes-phy', type=str, choices=['iwlwifi','rt2800usb'], default='iwlwifi', help='Wifi physical interface to use (selected by driver name: iwlwifi or rt2800usb)')

args = parser.parse_args()

if args.wifistandard == '11n':
    mcs = args.mcs
    channel = args.channel if args.channel != 999 else 140
elif args.wifistandard == '11g':
    ratekbps = args.ratekbps
    channel = args.channel if args.channel != 999 else 11

"Set output folder"
if not os.path.isdir(args.outdir):
    raise ValueError('Not a folder: {}'.format(args.outdir))

now = datetime.now().isoformat(timespec='seconds')
wlan_scans_folder = os.path.join(args.outdir, now)
print_t("Output folder is "+wlan_scans_folder)

"Update time on this system"
print_t("updating time on this system... ", end='')
set_date_from_ntp()
print("done")


"Get ssh connections to nodes"
clusters = []
# at first set a connection to all clusters. In case rt2800usb phy is chosen, afterwards sort out nodes that don't have it connected
if 'aa' in args.clusters:
    clusters.append(
        Cluster('aa', [
            ClusterNode('aa{}'.format(i+1),
                    '192.168.11.{}'.format(121+i),
                    '192.168.12.{}'.format(121+i),
                    username='ubuntu')
            for i in range(5)
            ]
        )
    )

if 'bb' in args.clusters:
    clusters.append(
        Cluster('bb', [
            ClusterNode('bb{}'.format(i+1), 
                    '192.168.11.{}'.format(126+i),
                    '192.168.12.{}'.format(126+i),
                    username='ubuntu')
            for i in range(5)
            ]
        )
    )

if 'cc' in args.clusters:
    clusters.append(
        Cluster('cc', [
            ClusterNode('cc{}'.format(i+1),
                    '192.168.11.{}'.format(131+i),
                    '192.168.12.{}'.format(131+i),
                    username='ubuntu')
            for i in range(5)
            ]
        )
    )

if 'dd' in args.clusters:
    clusters.append(
        Cluster('dd', [
            ClusterNode('dd{}'.format(i+1),
                    '192.168.11.{}'.format(136+i),
                    '192.168.12.{}'.format(136+i),
                    username='ubuntu')
            for i in range(5)
            ]
        )
    )

# sort out nodes that don't have rt2800usb connected
if args.nodes_phy == 'rt2800usb':
    def has_usbdrv(node, drivername):
        try:
            node.cmd('lsusb -t | grep rt2800usb')
            return True 
        except ExitcodeNotNullException:
            return False
    clusters = [Cluster(cl.name, [node for node in cl if has_usbdrv(node,'rt2800usb')] ) for cl in clusters]

"Get ssh connection to accesspoint"
accesspoint = Host('OpenWRT', '192.168.12.1', username='root')

"Initialize testbed"
boschbed = Testbed(clusters, accesspoint)

"Stop ptp4l and phc2sys"
boschbed.nodes_cmd('sudo systemctl stop ptp4l && sudo systemctl stop phc2sys')

"Clean up files and processes from previous simulations, if any"
boschbed.nodes_cmd('rm *.{json,pcap} || true; killall tcpdump || true')

"Set correct system clock on nodes"
print_t("set nodes system clock... ", end='')
boschbed.nodes_cmd('sudo timedatectl set-time "%s"' % datetime.now().strftime('%Y-%m-%d %H:%M:%S'))#, print_stdout=True)
print("done")

"Set AP configuration"
if args.wifistandard == '11n':
    print_t("setting AP config for 11n, ch {}, txpower {}, mcs {}... ".format(
        channel, args.txpower, mcs), end='')
    boschbed.ap_cmd(
        """uci batch <<-EOF
            set wireless.radio0.hwmode='11a'
            set wireless.radio0.require_mode='n'
            set wireless.radio0.htmode='HT20'
            set wireless.radio0.channel='{channel}'
            set wireless.radio0.txpower='{txpower}'
            set wireless.radio0.legacy_rates=0
            EOF
        """.format(channel=channel, txpower=args.txpower)
    )
    boschbed.ap_cmd('uci commit && wifi') # wifi is shorthand for wifi down && wifi up

    time.sleep(1)

    # check if txpower has successfully been set
    txpower_on_ap = float(boschbed.ap_cmd("iw dev | grep txpower | awk '{print $2}'"))
    if txpower_on_ap != args.txpower:
        raise ValueError('txpower {} could not be set: AP reports txpower {}'.format(
            args.txpower, txpower_on_ap))

    # need to call iw AFTER wifi down/up!
    cmds = [
        'iw dev wlan0 set bitrates ht-mcs-5', # clear bitrate mask
        'iw dev wlan0 set bitrates ht-mcs-5 {} sgi-5'.format(mcs)
    ]
    boschbed.ap_cmd(' && '.join(cmds))

    print("done")

elif args.wifistandard == '11g':
    print_t("setting AP config for 11g, ch {}, txpower {}, rate {}... ".format(
        args.channel, args.txpower, args.ratekbps), end='')
    boschbed.ap_cmd(
        """uci batch <<-EOF
            set wireless.radio0.hwmode='11g'
            set wireless.radio0.require_mode='g'
            set wireless.radio0.htmode='NONE'
            set wireless.radio0.channel='{channel}'
            set wireless.radio0.txpower='{txpower}'
            set wireless.radio0.basic_rate='{ratekbps}'
            set wireless.radio0.supported_rates='{ratekbps}'
            set wireless.radio0.legacy_rates=0
            EOF
        """.format(channel=channel, txpower=args.txpower, ratekbps=ratekbps)
    )
    boschbed.ap_cmd('uci commit && wifi') # wifi is shorthand for wifi down && wifi up

    time.sleep(1)

    # check if txpower has successfully been set
    txpower_on_ap = float(boschbed.ap_cmd("iw dev | grep txpower | awk '{print $2}'"))
    if txpower_on_ap != args.txpower:
        raise ValueError('txpower {} could not be set: AP reports txpower {}'.format(
            args.txpower, txpower_on_ap))

    print("done")
    
"Make scandummy interfaces on nodes"
if args.nodes_phy == 'rt2800usb':
    # limitation of rt2800usb driver: when creating a "managed" interface, the name is not recognized and defaults to some wlx... name
    ifname = "$(iw dev | grep wlx | awk '{print $2}')"
    # remove existing ones, if present
    boschbed.nodes_cmd("(for i in {}; do sudo iw dev $i del; done) || true".format(ifname))
    print_t("add scandummy interface on nodes using rt28000usb... ", end='')
    # select the correct 'phyX' interface by looking into the output of iw phy:
    #   Linksys rt2800usb:        'max # scan SSIDs: 4'
    #   Intel 3165 (iwlwifi): 'max # scan SSIDs: 20'
    boschbed.nodes_cmd("iw phy | grep 'max # scan SSIDs: 4' -B 1 | grep Wiphy | awk '{print $2}'"
        +"| xargs -I _ sudo iw _ interface add scandummy type managed && sleep .5 && sudo ip link set {} up".format(ifname))
    print("done")
    time.sleep(1)

else:
    # remove existing ones, if present
    boschbed.nodes_cmd("sudo iw dev scandummy del || true")
    print_t("add monitor interface on nodes using iwlwifi... ", end='')
    boschbed.nodes_cmd('sudo iw phy0 interface add scandummy type managed && sudo ip link set scandummy up')#, print_stdout=True)
    ifname = 'scandummy'
    print("done")
    time.sleep(1)

"Run wlan scan"
print_t("dump network scan on all nodes with iw <ifname> scan... ", end='')
boschbed.nodes_cmd('sudo iw {} scan -u > $(hostname)_wlan_scan_{}.log'.format(ifname, now))
boschbed.nodes_slurp('*wlan_scan*.log', wlan_scans_folder)
with open(os.path.join(wlan_scans_folder, '_wlan_scan.json'), 'w') as fd:
    json.dump({ # save a json file with datetime to be independent from unreliable ctime/mtime values
            'datetime': now,
            'clusters': args.clusters,
        },fd
    )
print("done")

"Remove scandummy interface"
print_t("removing the scandummy interface..", end='')
boschbed.nodes_cmd('sudo ip link set {ifname} down && sudo iw {ifname} del'.format(ifname=ifname))
time.sleep(1)
print("done")