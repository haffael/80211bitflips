#!/usr/bin/env bash
[[ -n "$WIRESHARK_PROFILES_DIR" && -d "$WIRESHARK_PROFILES_DIR" ]] || { WIRESHARK_PROFILES_DIR=~/.config/wireshark/profiles; mkdir -p -v $WIRESHARK_PROFILES_DIR; }
cmd="ln -s $(pwd)/wiresharkprofile_udp_only $WIRESHARK_PROFILES_DIR/udp_only"
echo "Making a symbolic link in wireshark profiles directory"
echo "> $cmd"
eval "$cmd"
