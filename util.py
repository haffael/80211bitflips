# helper class
# see https://code.activestate.com/recipes/52308-the-simple-but-handy-collector-of-a-bunch-of-named/?in=user-97991#c7
class Bunch(dict):
    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
        self.__dict__ = self

    def __getstate__(self):
        return self

    def __setstate__(self, state):
        self.update(state)
        self.__dict__ = self


from datetime import datetime        
from sys import stdout
def print_t(str, *args, **kwargs):
    print("[{}]".format(datetime.now().isoformat(timespec='seconds')), str, *args, **kwargs)
    stdout.flush()

def sprint_t(str):
    return "[{}] ".format(datetime.now().isoformat(timespec='seconds'))+str