#!/usr/bin/env python
import sys
import socket
import numpy as np
from time import sleep

UDP_IP = "127.0.0.1"
UDP_PORT = 34567
NUMBYTES = 1000 # payload size

try:   
    numpkg = int(sys.argv[1])
except:
    print("Send <numpkg> UDP packets with payload 1000 bytes and a few fake errors (1)")
    print("Modification: Non-uniform bitflip probability. Higher at beginning, lower at end")
    print("")
    print("usage: "+sys.argv[0]+" <numpkg> [<mean_bitflip_rate> [<slope>]]")
    print("  <mean_bitflip_rate> : float [ 0,1] mean bitflip rate")
    print("  <slope>             : float [-2,2] strength of non-uniformity")
    exit(1)

try: 
    mean_bitflip_rate = float(sys.argv[2])
except:
    mean_bitflip_rate = 0.1
    print("mean_bitflip_rate not given, setting to 0.1")

try:
    slope = float(sys.argv[3])
    if slope < -2.0 or slope > 2.0: raise ValueError
except KeyError:
    print("slope not given, setting to 1.0")
    slope = 1.0
except:
    print("slope must be a float number between -2.0 and 2.0.")
    exit(1)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) # allow broadcast

num_bits = NUMBYTES*8
# bitflip probability decreases linearly
bitflip_rates = np.linspace((1+slope/2)*mean_bitflip_rate, (1-slope/2)*mean_bitflip_rate, num=num_bits, endpoint=True)
for i in range(0,numpkg):
    # generate num_bits random floats between 0 and 1
    random_uniform = np.random.rand(num_bits)
    # when their value is lower than bitflip_rate, it's a flip
    bitflips_bool = random_uniform < bitflip_rates # compares element-wise
    # make bytes out of the bool array and send it
    sock.sendto(
            np.packbits(bitflips_bool).tobytes(),
            (UDP_IP, UDP_PORT)
    )

sock.close()
