#!/usr/bin/env python3
import sys
import os
import subprocess
import time
import numpy as np
import argparse
import json
import pandas as pd
import pickle

#from evalcaptures.capture import Capture
#from evalcaptures.captureset import CaptureSet, UnicastDestRecvTooFewException, UnicastDestCaptureNotFoundException
from evalcaptures.supercaptureset import SuperCaptureSet

from util import print_t, sprint_t

prac_decode_fromtrace = 'prac/bin/prac_decoder_fromtrace' # binary to call (on remote machine)
libkodoc_so_location = 'prac/bin/'

class NoPacketsCapturedException(Exception):
    pass

def prac_run(bin_abs_path, q, g, p, protocol, r, t):
    
    # check if file is empty
    # print("filesize", os.path.getsize(bin_abs_path))
    if os.path.getsize(bin_abs_path) == 0: 
        raise NoPacketsCapturedException()
    
    cmd = [prac_decode_fromtrace,
        '-i', bin_abs_path,
        '-q' , str(q),
        '-g', str(g),
        '-p', str(p),
        '--protocol', protocol,
        '--redundancy', str(r),
        '--prac-error-tolerance', str(t)
    ]
    
    try: 
        print_t('Calling '+' '.join(cmd)+' ...')
        stdout = subprocess.check_output(cmd, env={'LD_LIBRARY_PATH': libkodoc_so_location })
    except Exception as e: # @TODO !!!
        raise e

    print_t('Returned from call.')
    
    return json.loads(stdout.decode('utf-8'))



# from Argparse tutorial
parser = argparse.ArgumentParser(description='Batch PRAC evaluation for captures')
parser.add_argument('folders_exprs', type=str, nargs='+',
                    help='Folder(s) which capturesets to process (wildcards possible)')
parser.add_argument('--debug', action='store_const',
                    const=True, default=False,
                    help='Enable debug mode')
# parser.add_argument('--noconfirm', action='store_const',
#                     const=True, default=False,
#                     help='Supress confirmation dialog for foldernames')
parser.add_argument('--prac-fieldsize', '-q', type=int, nargs='*',
                    help='PRAC: Order of binary Golois field extension(s) to use, at the moment only 8 is supported (corresponding to binary8 field).',
                    default=[8])
parser.add_argument('--prac-generation-size', '-g', type=int, nargs='+',
                    help='PRAC: Generation size(s) to test')
parser.add_argument('--prac-pktlen', '-p', type=int, nargs='*',
                    help='PRAC: Packet size(s) in bytes when reading the raw binary .bin file (it is possible to pick a different size than the size of captured packets, but use with care..), default: 1400',
                    default=[1400])
parser.add_argument('--prac-protocol', type=str, nargs='+', choices=['reliable_arq', 'fixedrate'],
                    help='PRAC: Protcol(s) to use. Default: reliable_arq', 
                    default=['reliable_arq'])
parser.add_argument('--prac-redundancy', '-r', type=int, nargs='*',
                    help='PRAC: Redundancie(s) to use (for fixedrate). Default: 1. For reliable_arq, 0 is used regardless of this setting', 
                    default=[1])
parser.add_argument('--prac-error-tolerance', '-t', type=int, nargs='*',
                    help='PRAC: Error tolerance(s) to use. Default: 4', 
                    default=[4])
# TODO
# ("not-discard-broken-coeff", "do not discard received packets with broken coefficients (needing a god-view)")
# ("not-discard-broken-hash", "do not discard received packets with broken hash (needing a god-view), only with hash_in_pkt == false (see code)")
# ("daprac", "use DAPRAC instead of PRAC")
args = parser.parse_args()

print_t("Starting up, call was ", sys.argv)

debug = args.debug
if debug:
    print_t("Running in debug mode.")

folders_exprs = args.folders_exprs

# PRAC-specific variables
# TODO sanity checks
#q_list = args.prac_fieldsize #TODO
q_list = [8] 
g_list = args.prac_generation_size
p_list = args.prac_pktlen
protocol_list = args.prac_protocol
r_list = args.prac_redundancy
t_list = args.prac_error_tolerance


scs = SuperCaptureSet()
for folder_expr in folders_exprs:
    scs.add_capturesets(folder_expr)

captures_flat = [] # just all capture objects in one list
for captureset in scs.capturesets:
    for cap in captureset.captures:
        captures_flat.append(cap)

i = 0
for cap in captures_flat:
    i += 1
    print("[{}/{}] ch {}, {} {}, txpower {}, host {} ({} pkts, {} broken) ".format(
        i, len(captures_flat),
        cap.channel, cap.wifistandard, (cap.mcs if cap.wifistandard == '11n' else cap.ratekbps), cap.txpower, cap.host,
        cap.count_pkts, cap.count_badfcs
    ))
    # try: cap.prac_results
    # except AttributeError: 
    #     cap.prac_results = []

    cap_pracpklfilename = cap._json_abs_path.replace('.json', '.pracpkl')

    try:
        prac_df = pd.read_pickle(cap_pracpklfilename)
        print_t("{} previous results found".format(len(prac_df)))
    except FileNotFoundError:
        print_t("No previous results found, creating new DataFrame")
        prac_df = pd.DataFrame()

    for q in q_list:
        
        for g in g_list:
            
            for p in p_list:

                for protocol in protocol_list:

                    if protocol == 'reliable_arq': r_list_tmp = [0]
                    else: r_list_tmp = r_list

                    for r in r_list_tmp:

                        for t in t_list:
                            
                            # is there an entry already for this parameters?

                            if(len(prac_df) > 0): 

                                duplicate_df = prac_df.query('q == {q} and g == {g} and pktlen == {p} '
                                    'and protocol == "{protocol}" and redundancy == {r} and error_tolerance == {t}'
                                    .format(q=q, g=g, p=p, protocol=protocol, r=r, t=t)
                                    )
                                
                                if len(duplicate_df) > 0:
                                    print_t("already got       q={q}, g={g}, pktlen={pktlen}, protocol={protocol}, redundancy={redundancy}, error_tolerance={error_tolerance}: Decoded {bytes_decoded} kB".format(
                                        q=q, g=g, pktlen=p, protocol=protocol, redundancy=r, error_tolerance=t, bytes_decoded=duplicate_df['bytes_decoded'].iloc[0]
                                    ))
                                    continue
                            
                            # foundit = False
                            # for entry in cap.prac_results:
                            #
                            #     try:
                            #         foundit = (
                            #                 entry['q'] == q
                            #             and entry['g'] == g
                            #             and entry['pktlen'] == p
                            #             and entry['protocol'] == protocol
                            #             and entry['redundancy'] == r
                            #             and entry['error_tolerance'] == t
                            #         )
                            #     except KeyError:
                            #         foundit = False
                            #
                            # if(foundit):
                            #     print_t("already got       q={}, g={}, p={}, protocol={}, r={}, t={}: Decoded {} kB".format(
                            #         q, g, p, protocol, r, t,
                            #         (cap.prac_results[-1]['count_prac_generations_decoded'] * g 
                            #             * cap.prac_results[-1]['pktlen']) / 1000.0
                            #     ))
                            #     break # break loop over existing prac_results
                            #
                            #if foundit: continue # skip this parameter set in outer loop
                            
                            try:
                                res = prac_run(cap._bin_abs_path, q, g, p, protocol, r, t)
                                print_t("finished! Adding  q={q}, g={g}, pktlen={pktlen}, protocol={protocol}, redundancy={redundancy}, error_tolerance={error_tolerance}: Decoded {bytes_decoded} kB".format(
                                    q=res['q'], g=res['g'], pktlen=res['pktlen'], protocol=res['protocol'], redundancy=res['redundancy'],
                                    error_tolerance=res['error_tolerance'], bytes_decoded=res['bytes_decoded']
                                ))
                                prac_df = prac_df.append(res, ignore_index=True)
                                prac_df.to_pickle(cap_pracpklfilename) # save

                            except NoPacketsCapturedException:
                                print("(caught NoPacketsCapturedException) Creating dataframe with parameters and zero results")
                                prac_df = prac_df.append({
                                    "infile": cap._bin_abs_path,
                                    "q": q,
                                    "w": p,
                                    "g": g,
                                    "pktlen": p,
                                    "coeff_in_pkt": False,
                                    "hash_in_pkt": False,
                                    "discard_broken_coeff": True,
                                    "discard_broken_hash": True,
                                    "protocol": protocol,
                                    "redundancy": r,
                                    "error_tolerance": t,
                                    "count_bytes": 0,
                                    "count_flipped_bits": 0,
                                    "count_symbols": 0,
                                    "count_broken_symbols": 0,
                                    "count_broken_packets": 0, 
                                    "count_packets": 0,
                                    "sum_partial_packets": 0,
                                    "count_coeff_rx_corrupted": 0,
                                    "count_hash_rx_corrupted": 0,
                                    "count_repaired_packets": 0,
                                    "count_prac_not_needed": 0, 
                                    "count_prac_decoding_tries": 1613, 
                                    "count_prac_decoding_failed": 1488, 
                                    "count_prac_too_many_errors": 0,
                                    "count_prac_false_positives": 0,
                                    "count_prac_generations_decoded": 0,
                                    "bytes_decoded": 0,
                                    "time_total_sec": 0,
                                    "time_per_gen_mean_usec": 0,
                                }, ignore_index=True)
                                prac_df.to_pickle(cap_pracpklfilename) # save
                            


                    # cap.save_cache() # save the data to cache
                    # print_t("Saved to", cap._pkl_abs_path)

print_t("finished all jobs")
