#!/usr/bin/env python3
import sys
import socket

UDP_IP = "192.168.12.255"
UDP_PORT = 34567

try:   
    numpkg = int(sys.argv[1])
except:
    print("Send <numpkg> UDP packets with payload 1000 zero bytes")
    print("usage: "+sys.argv[0]+" <numpkg>")
    exit(1)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) # allow broadcast

print("sending {} packages with 1000 zero bytes to {}:{}".format(numpkg, UDP_IP, UDP_PORT))

for i in range(0,numpkg):
    sock.sendto(bytes(1000), (UDP_IP, UDP_PORT)) # bytes() with int argument makes zeros

sock.close()
