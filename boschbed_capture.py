#!/usr/bin/env python3
#import numpy as np
import time
import json
import socket
import os

import argparse
import subprocess

#from pprint import pprint
from datetime import datetime

from pycluster.host import Host, ExitcodeNotNullException, OutputTooLongError
from pycluster.clusternode import ClusterNode
from pycluster.cluster import Cluster
from pycluster.testbed import Testbed

from util import print_t, sprint_t, Bunch

"Update system time"
def set_date_from_ntp(): 
    subprocess.call(['sudo', 'ntpdate', 'time.zih.tu-dresden.de'])

parser = argparse.ArgumentParser(description='Boschbed batch capture')
# parser.add_argument('folders', metavar='folder_expr', type=str, nargs='+',
#                     help='Expression(s) which folders to process. Evaluated by ls -1d (wildcards possible). Be sure to include trailing slash.\n Default: This directory.')

parser.add_argument('--outdir', '-o', action='store', type=str,
                    default="",
                    help="Folder to put all capturesets in. Will be created if necessary. Variables will be substituted, e.g.\n"\
                        '{now.month:02d} {now.day:02d} {param_set.channel:02d} {param_set.numpkt_k} {param_set.pktdatalen} {param_set.mcs:02d} {param_set.txpower:02d}dBm"')
parser.add_argument('--folderformat', action='store', type=str,
                    default="{now.year:04d}-{now.month:02d}-{now.day:02d}T{now.hour:02d}:{now.minute:02d}:{now.second:02d}_{param_set.numpkt_k}k",
                    help='individual captureset foldernames. Variables will be substituted, e.g.\n'\
                        '{now.month:02d} {now.day:02d} {param_set.channel:02d} {param_set.numpkt_k} {param_set.pktdatalen} {param_set.mcs:02d} {param_set.txpower:02d}dBm"')
parser.add_argument('--noconfirm', action='store_const', const=True, default=False, help='Supress confirmation dialog for foldernames')
parser.add_argument('--noclocksync', action='store_const', const=True, default=False, help='Disable syncing of system clock (for debugging, because it takes a few seconds)')
parser.add_argument('--wifistandard', '-w', type=str, choices=['11n','11g'], default='11n', help='Standard (11g or 11n). Default: 11n' )
parser.add_argument('--broadcast', '-b', action='store_const', const=True, default=False, help='Use broadcast instead of unicast (short for dest-ip 192.168.12.255)')
parser.add_argument('--dest-ip', type=str, nargs='*', default=[''], help='Destination IP(s) for unicast. Default "192.168.12.145" (nc01)')
parser.add_argument('--dest-port', type=int, nargs='*', default=[34567], help='UDP destination port(s). Default: 34567')
parser.add_argument('--channel', '-ch', type=int, nargs='*', default=[999], help='Wifi channel(s). Default: 140 (11n), 11 (11g)')
parser.add_argument('--pktdatalen', '-plen', type=int, nargs='*', default=[1400], help='Length(s) of data packets in bytes. Default 1400') 
parser.add_argument('--numpkt', '-n', type=int, nargs='*', default=[10000], help='Number(s) of sent packets per capture')
parser.add_argument('--txpower', '-t', type=int, nargs='*', default=[5], help='Txpower(s) in dBm. Default 5')
parser.add_argument('--mcs', '-m', type=int, nargs='*', default=[15], help='MCS(s) for 11n. Default: 15')
parser.add_argument('--ratekbps', '-r', type=int, nargs='*', default=[6000], help='Rate in kbps for 11g. Default: 6000')
parser.add_argument('--pktsendtimedelta', '-pdelta', nargs='*', type=int, default=[10], help='Time(s) to sleep between sent packets in milliseconds. Default 10')
parser.add_argument('--multiply', '-mul', type=int, default=1, help='Number of times the parametersets are run, in an interleaved way')
parser.add_argument('--clusters', '-c', type=str, nargs='*', default=['aa', 'bb', 'cc', 'dd'], help='Cluster(s) to capture on. Default: aa bb cc dd')
parser.add_argument('--randomdata', action='store_const', const=True, default=False, help='send random data instead of zeros')
parser.add_argument('--nodes-phy', type=str, choices=['iwlwifi','rt2800usb'], default='iwlwifi', help='Wifi physical interface to use (selected by driver name: iwlwifi or rt2800usb)')

args = parser.parse_args()

params_wanted = Bunch()
params_wanted.wifistandard = args.wifistandard # for reference in json file
params_wanted.channel = args.channel
params_wanted.pktdatalen = args.pktdatalen
params_wanted.numpkt = args.numpkt
params_wanted.txpower = args.txpower
if args.wifistandard == '11n':
    params_wanted.mcs = args.mcs
    params_wanted.channel = args.channel if args.channel != [999] else [140]
elif args.wifistandard == '11g':
    params_wanted.ratekbps = args.ratekbps
    params_wanted.channel = args.channel if args.channel != [999] else [11]
if args.dest_ip == ['']:
    if args.broadcast:
        params_wanted.dest_ip = ['192.168.12.255']
    else:
        params_wanted.dest_ip = ['192.168.12.145']
else:
    params_wanted.dest_ip = args.dest_ip
params_wanted.broadcast = 'true' if args.broadcast else 'false' # for reference in json file - have to use these strings because itertools.product doesnt like boolean!
params_wanted.dest_port = args.dest_port
params_wanted.randomdata = 'true' if args.broadcast else 'false'  # for reference in json file - have to use these strings because itertools.product doesnt like boolean!
params_wanted.clusters = [args.clusters] # for reference in json file - wrapped in another list because the single list would get exploded by itertools.product
params_wanted.pktsendtimedelta = [x*0.001 for x in args.pktsendtimedelta] # convert to seconds

"Generate the template for folder naming (if not given by --outdir/-o)"
if args.outdir:
    outdir = args.outdir
else:
    outdir = 'ch{param_set.channel:03d}_{param_set.pktdatalen}B_'
    
    if args.wifistandard == '11n':
        outdir += "mcs{param_set.mcs:02d}_{param_set.txpower:02d}dBm_"
    elif args.wifistandard == '11g':
        outdir += "{param_set.ratekbps:02d}kbps_{param_set.txpower:02d}dBm_"
    
    outdir += 'broadcast' if args.broadcast else 'unicast'
    
    if args.randomdata:
        outdir = 'random_'+outdir
    else:
        outdir = 'zeros_'+outdir

local_folder_formatstr = os.path.join(outdir, args.folderformat)


"Explode the parameters to single directories with all combinations"
# https://stackoverflow.com/a/41254252
from itertools import product
params_wanted_flat = [[(k,vs)] if isinstance(vs,str) else [(k,v) for v in vs] for k, vs in params_wanted.items()]

param_sets = list(map(
        Bunch,
        product(*params_wanted_flat)
    ))

# convert 'true' and 'false' back to real booleans (replaced before because of itertools.product)
for param_set in param_sets:
    for k in param_set.__dict__:
        if k in ('broadcast', 'randomdata'):
            param_set[k] = (param_set[k] == 'true')

param_sets_mult = param_sets*args.multiply

"Display the parameter sets to the user and get confirmation"
from tabulate import tabulate
print(tabulate(param_sets_mult, headers='keys', showindex=True))

print("\nlocal folder is e.g.", local_folder_formatstr.format(
        param_set=Bunch(param_sets_mult[0], numpkt_k=int(param_sets_mult[0].numpkt/1000)),
        now=datetime.now()
    ))

"User confirmation before parsing"
if not args.noconfirm:
    userinput = input("\nIs this what you desire? [y/n]: ")
    userinput_str = str(userinput)
    if userinput_str == 'y':
        pass
    else:
        print("Aborting.")
        exit(1)

#param_sets_mult_bunches = [Bunch(**param_set) for param_set in param_sets_mult] # TODO umweg ueber dict weg, gleich bei bunch bleiben


clusters = []

# at first set a connection to all clusters. In case rt2800usb phy is chosen, afterwards sort out nodes that don't have it connected
if 'aa' in args.clusters:
    clusters.append(
        Cluster('aa', [
            ClusterNode('aa{}'.format(i+1),
                    '192.168.11.{}'.format(121+i),
                    '192.168.12.{}'.format(121+i),
                    username='ubuntu')
            for i in range(5)
            ]
        )
    )

if 'bb' in args.clusters:
    clusters.append(
        Cluster('bb', [
            ClusterNode('bb{}'.format(i+1), 
                    '192.168.11.{}'.format(126+i),
                    '192.168.12.{}'.format(126+i),
                    username='ubuntu')
            for i in range(5)
            ]
        )
    )

if 'cc' in args.clusters:
    clusters.append(
        Cluster('cc', [
            ClusterNode('cc{}'.format(i+1),
                    '192.168.11.{}'.format(131+i),
                    '192.168.12.{}'.format(131+i),
                    username='ubuntu')
            for i in range(5)
            ]
        )
    )

if 'dd' in args.clusters:
    clusters.append(
        Cluster('dd', [
            ClusterNode('dd{}'.format(i+1),
                    '192.168.11.{}'.format(136+i),
                    '192.168.12.{}'.format(136+i),
                    username='ubuntu')
            for i in range(5)
            ]
        )
    )

# sort out nodes that don't have rt2800usb connected
if args.nodes_phy == 'rt2800usb':
    def has_usbdrv(node, drivername):
        try:
            node.cmd('lsusb -t | grep rt2800usb')
            return True 
        except ExitcodeNotNullException:
            return False
    clusters = [Cluster(cl.name, [node for node in cl if has_usbdrv(node,'rt2800usb')] ) for cl in clusters]

accesspoint = Host('OpenWRT', '192.168.12.1', username='root')


"Initialize testbed"
boschbed = Testbed(clusters, accesspoint)

"Stop ptp4l and phc2sys"
boschbed.nodes_cmd('sudo systemctl stop ptp4l && sudo systemctl stop phc2sys')

# "Set one and only route to nuc"
# boschbed.nodes_cmd('sudo ip route del default')
# #boschbed.nodes_cmd('sudo ip route del 192.168.12.0/24 || true') # if it exists
# boschbed.nodes_cmd('sudo ip route add default via 192.168.11.100 dev enp0s31f6 metric 1000000')
    
"Clean up files and processes from previous simulations, if any"
boschbed.nodes_cmd('rm *.{json,pcap} || true; killall tcpdump || true')

if args.nodes_phy == 'rt2800usb':
    print_t("add monitor interface on nodes (call mon0start.sh) using rt28000usb... ", end='')
    # select the correct 'phyX' interface by looking into the output of iw phy:
    #   Linksys rt2800usb:        'max # scan SSIDs: 4'
    #   Intel 3165 (iwlwifi): 'max # scan SSIDs: 20'
    # and use it as an argument for mon0start.sh to make the interface mon0 on the right phy
    boschbed.nodes_cmd("iw phy | grep 'max # scan SSIDs: 4' -B 1 | grep Wiphy | awk '{print $2}' | xargs sudo 80211bitflips/mon0start.sh > mon0start.log")
    print("done")
    time.sleep(1)

else:
    print_t("add monitor interface on nodes (call mon0start.sh)... ", end='')
    # call mon0sstart.sh without argument, default is phy0
    boschbed.nodes_cmd('sudo 80211bitflips/mon0start.sh')#, print_stdout=True)
    print("done")
    time.sleep(1)


def capture(param_set, destination_local_folder):
    """do a capture with given parameter set and slurp it to <destination_local_folder>
    """

    "update system time here"
    if not args.noclocksync:
        print_t("updating time on this system... ", end='')
        set_date_from_ntp()
        print("done")
    
    "Set AP configuration"
    if param_set.wifistandard == '11n':
        print_t("setting AP config for 11n, ch {}, txpower {}, mcs {}... ".format(
            param_set.channel, param_set.txpower, param_set.mcs), end='')
        boschbed.ap_cmd(
            """uci batch <<-EOF
                set wireless.radio0.hwmode='11a'
                set wireless.radio0.require_mode='n'
                set wireless.radio0.htmode='HT20'
                set wireless.radio0.channel='{0.channel}'
                set wireless.radio0.txpower='{0.txpower}'
                set wireless.radio0.legacy_rates=0
                EOF
            """.format(param_set)
        )
        boschbed.ap_cmd('uci commit && wifi') # wifi is shorthand for wifi down && wifi up

        time.sleep(1)

        # check if txpower has successfully been set
        txpower_on_ap = float(boschbed.ap_cmd("iw dev | grep txpower | awk '{print $2}'"))
        if txpower_on_ap != param_set.txpower:
            raise ValueError('txpower {} could not be set: AP reports txpower {}'.format(
                param_set.txpower, txpower_on_ap))

        # need to call iw AFTER wifi down/up!
        cmds = [
            'iw dev wlan0 set bitrates ht-mcs-5', # clear bitrate mask
            'iw dev wlan0 set bitrates ht-mcs-5 {} sgi-5'.format(param_set.mcs)
        ]
        boschbed.ap_cmd(' && '.join(cmds))

        print("done")

    elif param_set.wifistandard == '11g':
        print_t("setting AP config for 11g, ch {}, txpower {}, rate {}... ".format(
            param_set.channel, param_set.txpower, param_set.ratekbps), end='')
        boschbed.ap_cmd(
            """uci batch <<-EOF
                set wireless.radio0.hwmode='11g'
                set wireless.radio0.require_mode='g'
                set wireless.radio0.htmode='NONE'
                set wireless.radio0.channel='{0.channel}'
                set wireless.radio0.txpower='{0.txpower}'
                set wireless.radio0.basic_rate='{0.ratekbps}'
                set wireless.radio0.supported_rates='{0.ratekbps}'
                set wireless.radio0.legacy_rates=0
                EOF
            """.format(param_set)
        )
        boschbed.ap_cmd('uci commit && wifi') # wifi is shorthand for wifi down && wifi up

        time.sleep(1)

        # check if txpower has successfully been set
        txpower_on_ap = float(boschbed.ap_cmd("iw dev | grep txpower | awk '{print $2}'"))
        if txpower_on_ap != param_set.txpower:
            raise ValueError('txpower {} could not be set: AP reports txpower {}'.format(
                param_set.txpower, txpower_on_ap))

        print("done")

    if not param_set.broadcast:
        unicast_dest = Host('nc01', '192.168.11.145', username='user')
        print_t("make sure unicast destination is connected... ", end='')
        tries = 0; max_tries = 10; try_sleep_secs = 5
        unicast_dest_mac = '00:c2:c6:f4:eb:65' #TODO remove hardcoded MAC
        while True:
            time.sleep(try_sleep_secs)
            try:
                tries += 1
                # check ap status if connected
                boschbed.ap_cmd('iw wlan0 station dump | grep "{}"'.format(unicast_dest_mac))
                # check sta status if connected
                unicast_dest.cmd('iw wlp1s0 link | grep SSID')
                # check sta IP
                unicast_dest.cmd('ip a | grep wlp1s0 | grep 145')#, print_stdout=True) #TODO remove hardcoded IP!
                # no exception means successful
                break
            except ExitcodeNotNullException as e:
                print_t("\n try {}: {} failed with exitcode {}, retrying... ".format(tries, e.cmd, e.exitcode), end='')
                if tries >= max_tries:
                    print_t('\nunicast_dest did not connect to AP after waiting for {} seconds'.format(tries*try_sleep_secs))
                    raise e
                # else: continue loop
        print("done")
        

    print_t("set nodes system clock... ", end='')
    boschbed.nodes_cmd('sudo timedatectl set-time "%s"' % datetime.now().strftime('%Y-%m-%d %H:%M:%S'))#, print_stdout=True)
    if not param_set.broadcast:
        unicast_dest.cmd('sudo timedatectl set-time "%s"' % datetime.now().strftime('%Y-%m-%d %H:%M:%S'))#, print_stdout=True)
    print("done")

    print_t("set nodes to listen to the right channel (call mon0ch.sh)... ", end='')
    # select channel
    tries = 0; max_tries = 10; try_sleep_secs = 3
    while True:
        time.sleep(try_sleep_secs)
        try: 
            tries += 1
            boschbed.nodes_cmd('sudo 80211bitflips/mon0ch.sh {}'.format(param_set.channel))#, print_stdout=True)
            break
        except ExitcodeNotNullException as e:
            print_t('try {}/{}: {}'.format(tries, max_tries, str(e)))
            if tries >= max_tries:
                print_t('failed after {} tries')
    # check that correct channel is set
    tries = 0; max_tries = 20; try_sleep_secs = 10
    while True:
        time.sleep(try_sleep_secs)
        try:
            tries += 1
            boschbed.nodes_cmd('iw dev | grep "channel {}"'.format(param_set.channel))
            break
        except ExitcodeNotNullException as e:
            print_t('try {}/{}: {}'.format(tries, max_tries, str(e)))
            if tries >= max_tries:
                print_t('failed after {} tries'.format(tries))
                e.node.cmd('iw dev', print_stdout=True)
                raise e
    print("done")

    "start tcpdump captures"
    print_t("starting tcpdump captures... ", end='')
    filename_suffix = '{isodatetime}'.format(isodatetime=datetime.now().isoformat(timespec='seconds'))
    capture_sessions = boschbed.nodes_sessioncmd('sudo 80211bitflips/mon0capture.sh "wlan src 00:0f:c9:14:eb:44" '+filename_suffix)# 'udp && port 34567'")
    # add unicast_dest capture to this array as well
    if not params_wanted.broadcast:
        capture_sessions[unicast_dest] = unicast_dest.sessioncmd('sudo 80211bitflips/wlp1s0capture.sh "udp dst port {}" '.format(
            param_set.dest_port)+filename_suffix, cd_folder='raphael')
    print("done")
    time.sleep(3)

    #TODO check that captures are running: ps aux | grep tcpdump | grep -v grep OR BETTER with given PIDs

    "send udp packets"
    print_t("sending {} UDP packets to {}:{} with {} sec pause in between... ".format(param_set.numpkt, param_set.dest_ip, param_set.dest_port, param_set.pktsendtimedelta), end='')
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) # allow broadcast

    for _ in range(0,param_set.numpkt):
        sock.sendto(
                #bytes([int(i/256), i % 256]+[0]*(pktdatalen-2)), # send increasing numbers in the first two bytes, rest zeros
                os.urandom(param_set.pktdatalen) if args.randomdata else bytes([0]*param_set.pktdatalen),
                (param_set.dest_ip, param_set.dest_port)
        )
        # pause to avoid txqueue overflow
        time.sleep(param_set.pktsendtimedelta)
    print("done")

    sock.close()

    "kill the capture processes"
    time.sleep(3)
    print_t("killing the capture processes... ", end='')
    Testbed.kill_sessions(capture_sessions)#, print_stdout=True)

    #run.cmd('sleep .5 && ls | grep pcap', print_stdout=True)
    print("done")
    
    "get the captured data from the cluster nodes"
    print_t("getting the captured data from the cluster nodes... ", end='')
    time.sleep(1)
    boschbed.nodes_slurp('*.{pcap,json}', destination_local_folder)
    if not params_wanted.broadcast: 
        unicast_dest.slurp('raphael/*.{pcap,json}', destination_local_folder)
    print("done")

    "Querying tc on accesspoint to check for queue overflows"
    print_t("Querying tc on accesspoint to check for queue overflows... ", end='')
    param_set.tc_sentbytes, param_set.tc_sentpkt, param_set.tc_dropped, param_set.tc_overlimits, param_set.tc_requeues = [
        int(x) for x in 
        boschbed.ap_cmd("./tc -s -d class show dev wlan0 | grep 'mq :3' -A 1 | grep Sent | awk -F '[\ |,|\)]' '{print $3,$5,$8,$11,$13}'")
        .split()
    ]
    print("done")
    if param_set.tc_dropped > 0:
        print(
            "Warning: Queue overflow!\n  Sent bytes: {0.tc_sentbytes}\n  Sent packets: {0.tc_sentpkt}\n  "
            "Dropped: {0.tc_dropped}\n  Overlimits: {0.tc_overlimits}\n  Requeues: {0.tc_requeues}"
            .format(param_set)
        )

    "write _captureset.json into destination folder"
    with open(os.path.join(destination_local_folder, '_captureset.json'), 'w') as fd:
        json.dump(param_set, fd)
    

while True:
    try:
        cur_param_set = param_sets_mult.pop() # raises IndexError
    except IndexError:
        print("finished all captures")
        break

    cur_local_folder = local_folder_formatstr.format(
        param_set=Bunch(**cur_param_set, numpkt_k=int(cur_param_set.numpkt/1000)), # add numpkt_k property
        now=datetime.now()
    )
    print_t("starting next capture with parameters: ", cur_param_set)
    print_t("going to put this in local folder ", cur_local_folder)
    try:
        capture(
            cur_param_set,
            cur_local_folder
        )
    except Exception as e:
        #print_t("\nCaught Exception. Re-adding the current param_set to the queue:\n"+str(param_set))
        raise e
    print_t("finished capture")

# "copy to zbox"
# zbox_dir = '/mnt/archive/captures'
# print(
#     subprocess.check_output(
#         "ssh rave@192.168.157.246 'cd {} && rsync -rltD --progress \"nuc@nuc:~/bigdata/raphael/captures/*\" .'".format(
#             zbox_dir),
#         shell=True
#     ).decode('utf-8')
# )

"delete monitor interfaces and restore wlp2s0"
boschbed.nodes_cmd('sudo iw mon0 del && sudo iw phy0 interface add wlp2s0 type managed && sudo ip link set wlp2s0 up') # raises ExitcodeNotNullException
