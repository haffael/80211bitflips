#!/usr/bin/env python3
import sys
import socket
from time import sleep

UDP_IP = "192.168.12.255"
UDP_PORT = 34567

try:   
    numpkg = int(sys.argv[1])
except:
    print("Send a UDP packet with 1 byte payload every second.")
    print("usage: "+sys.argv[0]+" <numpkg>")
    exit(1)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

# send 256 packets with payload 1 byte
for i in range(0,255):
    sock.sendto(bytes([i]), (UDP_IP, UDP_PORT)) 
    sleep(1)

sock.close()
