import argparse
import os
import subprocess
import time
from glob import glob
from datetime import datetime

from pycluster.host import Host, ExitcodeNotNullException, OutputTooLongError
from pycluster.clusternode import ClusterNode
from pycluster.cluster import Cluster
from evalcaptures.captureset import CaptureSet

from util import print_t, sprint_t

parser = argparse.ArgumentParser(description='Cluster-wide capture processing. Calls batchparse.py on clusternodes.')
parser.add_argument('folders_exprs', metavar='folders_exprs', type=str, nargs='+', default=['.'],
                    help='Expression(s) which folders to process. Be sure to include trailing slash.\n Default: This directory.')

parser.add_argument('--debug', action='store_const', const=True, default=False, help='Enable debug mode')

parser.add_argument('--append', action='store_const', const=True, default=False, help='Ignore _prac_by marker in capturesets')

# clusters to put work on
parser.add_argument('--clusters', '-c', type=str, nargs='*', default=['aa', 'bb', 'cc', 'dd'], help='Cluster(s) to put work on. Default: aa bb cc dd')
parser.add_argument('--diy', action='store_const', const=True, default=False, help='Do it yourself: Override cluster setting and use local machine (still over ssh, make sure key is installed: `ssh-copy-id localhost`)')

(args, pass_args) = parser.parse_known_args() # known args are parsed, the rest are returned as list

debug = args.debug
if debug:
    print("Running in debug mode.")
    print_debug = lambda s, *args, **kwargs: print(s, *args, **kwargs)
    print_t_debug = lambda s, *args, **kwargs: print_t(s, *args, **kwargs)
else:
    print_debug = lambda s, *args, **kwargs: None
    print_t_debug = lambda s, *args, **kwargs: None

folderpaths_glob = [f_result for folder_expr in args.folders_exprs for f_result in glob(folder_expr)]

# exclude directories with _prac_by marker
if(args.append):
    folderpaths = folderpaths_glob
else:
    # exclude directories with _prac_by marker
    folderpaths = [f for f in folderpaths_glob if not glob(os.path.join(f,'_prac_by_*'))]

print('Folders to parse:\n  '+'\n  '.join(folderpaths))
print('')

# exclude directories without _captureset.json
folderpaths = [f for f in folderpaths if os.path.isfile(os.path.join(f,'_captureset.json'))]

# print excluded dirs (debug)
print_debug("excluded folders: ")
excl = set(folderpaths_glob) - set(folderpaths)
print_debug('\n  '.join(list(excl)) if len(excl) != 0 else '  None')

if args.diy:

    nodes = [
        ClusterNode('localhost', 'localhost', 'localhost', username='rave'),
        #ClusterNode('localhost', 'localhost', 'localhost', username='rave') # two cores
    ]

else:

    chosen_clusters = args.clusters

    # make connections
    #TODO put that part somewhere common for all scripts
    clusters = []
    if 'aa' in chosen_clusters:
        clusters.append(
            Cluster('aa', [
                ClusterNode('aa{}'.format(i+1),
                        '192.168.11.{}'.format(121+i),
                        '192.168.12.{}'.format(121+i),
                        username='ubuntu')
                for i in range(5)
                ]
            )
        )

    if 'bb' in chosen_clusters:
        clusters.append(
            Cluster('bb', [
                ClusterNode('bb{}'.format(i+1), 
                        '192.168.11.{}'.format(126+i),
                        '192.168.12.{}'.format(126+i),
                        username='ubuntu')
                for i in range(5)
                ]
            )
        )

    if 'cc' in chosen_clusters:
        clusters.append(
            Cluster('cc', [
                ClusterNode('cc{}'.format(i+1),
                        '192.168.11.{}'.format(131+i),
                        '192.168.12.{}'.format(131+i),
                        username='ubuntu')
                for i in range(5)
                ]
            )
        )

    if 'dd' in chosen_clusters:
        clusters.append(
            Cluster('dd', [
                ClusterNode('dd{}'.format(i+1),
                        '192.168.11.{}'.format(136+i),
                        '192.168.12.{}'.format(136+i),
                        username='ubuntu')
                for i in range(5)
                ]
            )
        )

    # shorthand
    nodes = [ node for cluster in clusters for node in cluster ]

print("Got {} nodes to work on.".format(len(nodes)))

# from itertools import cycle
# nodes_cycle = cycle(nodes)

# from collections import defaultdict
# jobs_per_node = defaultdict(list)
# for folderpath in folderpaths:
#     jobs_per_node[next(nodes_cycle)].append((folderpath)

# sessions = dict()
# for node, jobs in jobs_per_node:
#     # sessioncmd is non-blocking and returns the session
#     sessions[node] = node.sessioncmd('echo "{}"'.format(' '.join(jobs)))

"Create a queue and len(nodes) threads, each picking folderpaths from queue until empty"
import queue, threading
# adopted from python3.5 docs (Queue)
def worker(node):
    while True:
        folderpath = q.get()
        if folderpath is None:
            break # we are finished

        basename = os.path.basename(folderpath)
        remote_path = 'prac_workdir/'+basename
        print_t_debug('remote_path is {}'.format(remote_path))

        "sync the folder to node"
        print_t_debug('{}: pushing nuc:{} to remote:{}'.format(node, folderpath, remote_path))
        node.put(folderpath, remote_path)

        "prac the folder"
        node.cmd('echo "{}" >> prac.log'.format(
            sprint_t("(nuc datetime and message) Transferred {}".format(folderpath))
        ))
        prac_cmd = '80211bitflips/batchprac.py "{}/" '.format(remote_path) +\
            ' '.join(pass_args) +\
            ' >> prac.log'
        print_t_debug('{}: {}'.format(node, prac_cmd))
        node.cmd(prac_cmd)
        print_t_debug('{} returned from batchprac.py call'.format(node))

        "add the _prac_by marker (and delete all previous markers)"
        node.cmd('rm {remote_path}/_prac* || true; echo "{now}" > {remote_path}/_prac_by_{hostname}'.format(
            now=datetime.now().isoformat(),
            remote_path=remote_path,
            hostname=node.hostname))
        print_t_debug('{} added {}/_prac_by_{}'.format(node, remote_path, node.hostname))

        "sync the results back and delete data on remote"
        # avoid unspecific SCPException, instead make sure that some files exist before calling node.slurp().
        try:
            try:
                node.cmd('ls '+remote_path+'/{_prac*,*.pracpkl}') # order important (-> bash brace expansion): missing _prac* fails the command, regardless of *.pracpkl
            except OutputTooLongError:
                print_t_debug(node.hostname+': got OutputTooLongError with ls ...{_prac*,*.pracpkl} command')
                pass # doesnt matter
        except ExitcodeNotNullException as e:
            print_t_debug(str(node)+' ls '+remote_path+'/{_prac*,*.pracpkl}'+' returned {}'.format(e.exitcode))
            print_t_debug(str(node)+" Missing either _prac_by_<hostname> or at least one .pracpkl file. Need both!")
            print_t_debug(str(node)+' parsing failed. Take a look to see whats wrong: {}'.format(remote_path))
        else:
            print_t_debug('slurping back _prac* markers and pkl files to '+folderpath)
            node.slurp(remote_path+'/{_prac*,*.pracpkl}', folderpath, delete=False)
            node.cmd('rm -r '+remote_path)

        "send finished signal and get new folder, if there are some left"
        print_t('{}: finished {}'.format(node, folderpath))
        q.task_done()

q = queue.Queue()
threads = []
for node in nodes:
    t = threading.Thread(target=worker, args=[node])
    t.start()
    threads.append(t)

for folderpath in folderpaths:
    print_t_debug("adding {} to queue".format(folderpath))
    q.put(folderpath)

# block until all tasks are done
q.join()

# stop workers
for node in nodes:
    q.put(None)
for t in threads:
    t.join()

print("Queue completed.")

#TODO close connections
