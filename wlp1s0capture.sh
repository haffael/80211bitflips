#!/bin/bash
LOGFILE="/home/user/raphael/wlp1s0capture.log"
exec &> $LOGFILE # stdout and stderr to logfile
echo "[Debug info]: Called with parameters $@"
if ! [ $# -ge 2 ]; then
	cat <<-USAGE_EOF

	usage: wlp1s0capture <capture_filter> <filename>
	make a tcpdump capture from interface wlp1s0 with given capture_filter and save to
	<hostname>_<filename>.pcap.
	stop it with SIGINT (e.g. Ctrl+C).
	
	Also creates a corresponding <hostname>_<filename>.json file with some
	metadata
	USAGE_EOF
	exit 1
fi
#cdate="$(date +'%Y-%m-%d')"
#ctime="$(date +'%H-%m-%S')"
isodatetime="$(date --iso-8601=seconds)" # with second precision
host="$(hostname)"
iwfreq_line="$(iw dev | grep channel | xargs echo)" # xargs trims whitespace
iwchannel="$(echo "$iwfreq_line" | awk '{print $2}')" # channel number
if [ -z "$iwchannel" ]; then
	echo "[Debug info] Could not read channel!"
	exit 1
fi

filename=$host'_ch'$iwchannel'_'$2 # without ext
capture_filter="$1"
parameters=(-i wlp1s0 -w "$filename.pcap" -U "$capture_filter")
cat <<EOF_JSON > "$filename.json"
{
	"host": "$host",
	"iwfreq_line": "$iwfreq_line",
	"iwchannel": $iwchannel,	
	"pcap_filename": "$filename.pcap",
	"capture_filter": "${capture_filter//'"'/'\"'}",
	"tcpdump_cmd": "tcpdump ${parameters[@]//'"'/'\"'}",
	"isodatetime": "$isodatetime"
}
EOF_JSON
exec tcpdump ${parameters[@]} 2>&1 > >(tee -a $LOGFILE)
