#!/usr/bin/env python3
import sys
import os
import subprocess
import time
import numpy as np
import argparse

from evalcaptures.capture import Capture
from evalcaptures.captureset import CaptureSet, UnicastDestRecvTooFewException, UnicastDestCaptureNotFoundException

from util import print_t, sprint_t

# from Argparse tutorial
parser = argparse.ArgumentParser(description='Batch processing for captures')
parser.add_argument('folders_exprs', metavar='folders_expr', type=str, nargs='+',
                    help='Expression(s) which folders to process. Evaluated by ls -1d (wildcards possible). Be sure to include trailing slash.\n Default: This directory.')
parser.add_argument('--debug', action='store_const',
                    const=True, default=False,
                    help='Enable debug mode')
parser.add_argument('--nocache', action='store_const',
                    const=True, default=False,
                    help='Re-parse all captures (do not use cached pickle files)')
parser.add_argument('--threads', action='store',
                    type=int, default=2,
                    help='specify number of threads to be spawned (default: 2)')
parser.add_argument('--noconfirm', action='store_const',
                    const=True, default=False,
                    help='Supress confirmation dialog for foldernames')
args = parser.parse_args()

print_t("Starting up, call was ", sys.argv)

debug = args.debug
if debug:
    print_t("Running in debug mode.")

nocache = args.nocache
if nocache and not args.noconfirm:
    userinput = input("Re-parsing all captures. Really? [y/n]: ")
    userinput_str = str(userinput)
    if userinput_str == 'y':
        pass
    else:
        print_t("Aborting.")
        exit(1)

folderpaths = []
for folders_expr in args.folders_exprs:
    if not folders_expr.endswith('/'):
        print_t("Expression '{}' has no trailing slash! Aborting.".format(folders_expr))
        exit(1)
    try:
        folderpaths += subprocess.check_output('ls -1d {}'.format(folders_expr), shell=True).decode('utf8').splitlines()
    except subprocess.CalledProcessError:
        print_t("Resolving the expression '{}' failed.".format(folders_expr))
        exit(1)

threads = args.threads
print_t("running with {:d} threads".format(threads))

# static
#folderpaths = subprocess.check_output('ls -1d ../captures/testcaptures_batch_reparse2018-05-03/2018-04-27_ch52_unicast_10k*', shell=True).decode('utf8').splitlines()

print_t('Folders to parse:\n')
print('  '+'\n  '.join(folderpaths))
print('')

if not args.noconfirm:
    userinput = input("Is this what you desire? [y/n]: ")
    userinput_str = str(userinput)
    if userinput_str == 'y':
        pass
    else:
        print_t("Aborting.")
        exit(1)

max_caught_exceptions = 10
parsed_folders = 0
for foldernum, folderpath in enumerate(folderpaths):
    maxdecimals = int(np.log10(len(folderpaths))+1)
    print_t('Starting folder [{: _d}/{: _d}] {}'.replace(': _d', ': {}d'.format(maxdecimals)).format(foldernum+1, len(folderpaths), folderpath))
    log = []
    while True:
        ## DONT DO THAT - breaks multiprocessing stuff!
        #print("Killing old tshark processes..")
        #print(subprocess.check_output('killall tshark 2> /dev/null || echo "no tshark processes to kill"', shell=True).decode('utf8'))
        time.sleep(.5)
        
        try:
            cs = CaptureSet(folderpath, nocache=nocache, debug=debug, threads=threads)
            cs.parse()
            parsed_folders += 1
            break
        #except UnicastDestCaptureNotFoundException as e:
        #    print_t('No capture from unicast destination in this captureset')    
        except UnicastDestRecvTooFewException as e:
            print_t('got UnicastDestRecvTooFewException:\n'+str(e))
            break
        except Exception as e: # when an exception reaches this level, it's been severe - otherwise it would have been handles in CaprtureSet!
            print_t('CaptureSet raised {}: "{}",  (exception count: {})'.format(type(e), str(e), len(log)))
            log.append(sprint_t(str(e)))
            if len(log) > max_caught_exceptions: 
                print_t("stopped parsing folder {} because maximum number of caught exceptions ({}) is reached".format(folderpath, max_caught_exceptions))
                print_t("Printing the exception log:")
                print('  '+'\n  ------  \n'.join(log))
                print_t("Re-raising the exception now.")
                raise e
        
    
                
print_t('Parsed {} of {} folders.'.format(parsed_folders, len(folderpaths)))
