#!/usr/bin/env bash
if ! [ $# -eq 1 ]; then
	echo "Sets channel on monitor interface mon0"
	echo "usage: mon0ch <ch>"
       	exit 1
fi
#echo "> ip link set mon0 down"
#ip link set mon0 down || exit 1
echo "> iw dev mon0 set channel $1"
iw mon0 set channel $1 || exit 1
echo "> ip link set mon0 up"
ip link set mon0 up
echo "> iw dev | grep channel | xargs"
iw dev | grep channel | xargs
