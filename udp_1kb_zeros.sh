#!/usr/bin/env bash
if ! [ $# -eq 1 ]; then
	echo "Send <numpkg> UDP packets with payload 1000 zero bytes"
	echo "usage: $0 <numpkg>"
   	exit 1
fi

UDP_IP='127.0.0.1'
UDP_PORT=34567

# that doesnt work yet
mkfifo mypipe
nc -u $UDP_IP $UDP_PORT - < mypipe & # open socket
pid=$!
for i in `seq $1`; do dd if=/dev/zero bs=1000 count=1 && sleep 1 > mypipe 2> /dev/null; sleep 1; done
kill -INT $pid # kill nc
rm mypipe # remove named pipie
