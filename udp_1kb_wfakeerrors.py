#!/usr/bin/env python
import sys
import socket
import numpy as np
from time import sleep

UDP_IP = "127.0.0.1"
UDP_PORT = 34567
NUMBYTES = 1000 # payload size

try:   
    numpkg = int(sys.argv[1])
except:
    print("Send <numpkg> UDP packets with payload 1000 bytes and a few fake errors (1)")
    print("usage: "+sys.argv[0]+" <numpkg> [<bitflip_rate>]")
    exit(1)

try: 
    bitflip_rate = float(sys.argv[2])
except:
    bitflip_rate = 0.1
    print("bitflip_rate not given, setting to 0.1")

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) # allow broadcast

num_bits = NUMBYTES*8
for i in range(0,numpkg):
    # generate num_bits random floats between 0 and 1
    random_uniform = np.random.rand(num_bits)
    # when their value is lower than bitflip_rate, it's a flip
    bitflips_bool = random_uniform < bitflip_rate
    # make bytes out of the bool array and send it
    sock.sendto(
            np.packbits(bitflips_bool).tobytes(),
            (UDP_IP, UDP_PORT)
    )

sock.close()
