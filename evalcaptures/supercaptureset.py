import pandas as pd
import os.path
import dateutil.parser

from glob import glob
from pprint import pprint

from evalcaptures.captureset import CaptureSet, UnicastDestRecvTooFewException
from util import print_t # needs util.py in cwd

class SuperCaptureSet():

    def __init__(self, debug=False):

        self.debug = debug

        if debug:
            print("Running in debug mode.")
            self.print_debug = lambda s, *args, **kwargs: print(s, *args, **kwargs)
            self.print_t_debug = lambda s, *args, **kwargs: print_t(s, *args, **kwargs)
        else:
            self.print_debug = lambda s, *args, **kwargs: None
            self.print_t_debug = lambda s, *args, **kwargs: None

        self.capturesets = []

    def add_capturesets(self, folder_expr, debug=False):

        debug = self.debug | debug

        folderpaths_glob = set((f for f in glob(folder_expr)))

        exkl = dict()

        # exclude folders with packet count mismatch
        exkl['unicast_dest_mismatch'] = set((f for f in folderpaths_glob if os.path.isfile(
            os.path.join(f, '_parse_unicast_dest_pktmismatch')
        )))
        # exclude folders without _captureset.json
        exkl['no_captureset_json'] = set((f for f in folderpaths_glob if not os.path.isfile(
            os.path.join(f, '_captureset.json')
        )))

        exkl_all = set.union(*exkl.values())
        print_t(
            "Excluding {} capturesets:\n  ".format(len(exkl_all))\
            +'\n  '.join(('{count}: {key}'.format(count=len(exkl[key]), key=key) for key in exkl))
        )

        folderpaths = (folderpaths_glob - exkl_all)
        print_t("Loading: %d" % len(folderpaths))

        if len(folderpaths) == 0: 
            print("No captures left - nothing to load.")
            return

        for f in (folderpaths_glob - exkl_all):
            try: 
                cs = CaptureSet(f, debug=debug)
                cs.parse()
                self.capturesets.append(cs)
            except UnicastDestRecvTooFewException: 
                raise
                #continue # just skip

        print_t("loaded {} capturesets".format(len(self.capturesets)))

        "take parameters from the first captureset and check if all others match"
        self.param_set = {k:v for k,v in self.capturesets[0].__dict__.items() if k in [
            'channel', 'dest_ip', 'dest_port', 'display_filter', 'mcs', 'pktdatalen', 'pktsendtimedelta', 'txpower'
        ]}

        # no need for checking!
        
        # for cs in capturesets:
        #     if any((self.param_set[k] != getattr(cs,k) for k in self.param_set)):
        #         raise ValueError("Error: Capturesets don't have the same parameter set")
        # print("All needed parameters for concatenation are equal among the capturesets. Continuing")

    
    def build_df(self):

        "Generate pandas dataframes"
        self.cs_dfs = dict() # for hierarchical index later
        #cs_dfs = [] # for plain concat
        for cs in self.capturesets:
            "import all object attributes into a pandas dataframe, but without underscore attributes"
            cs_df = pd.DataFrame((
                {k:v for k, v in c.__dict__.items() if not k.startswith('_')}
                for c in cs.captures
            ))

            "also import the captureset data to an object attribute"

            # extra column for cluster for more convenient grouping
            cs_df['cluster'] = cs_df['host'].apply(lambda host: host[:2])

            cs_df.set_index('host', inplace=True) # discard numerical index

            # datetime column as pandas datetime
            cs_df['datetime'] = pd.to_datetime(cs_df['isodatetime']) # smart enough to detect iso format
            
            # cs_df['relnetbytes'] = cs_df['count_netbytes']/(cs_df['numpkt']*cs_df['pktdatalen'])
            # cs_df['relbitflips'] = cs_df['count_bitflips']/(cs_df['count_netbytes']*8)
            # #cs_df['partialbitflips'] = cs_df['count_bitflips']/(cs_df['count_badfcs'])
            # cs_df['relpartialbitflips'] = cs_df['count_bitflips']/(cs_df['count_badfcs']*cs_df['pktdatalen']*8)
            # #cs_df['date_short'] = cs_df['isodatetime'].apply(lambda x: dateutil.parser.parse(x).strftime('%a %b %d %H:%M'))


            # assign to dict with all capturesets
            self.cs_dfs[cs.basename] = cs_df # for hierarchical index
            #cs_dfs.append(cs_df) # for plain concat
            
        "build a combined dataframe"
        self.df = pd.concat(self.cs_dfs, names=['captureset', 'host']) # with correct names for the levels of the resulting MultiIndex
