#!/usr/bin/env python3
import os
import json
import subprocess

from multiprocessing.dummy import Pool as ThreadPool 
from multiprocessing.dummy import Lock
from datetime import datetime
from glob import glob
from traceback import format_exc

from .capture import Capture
from util import print_t

UNICAST_DEST_HOSTNAME = 'nc01'

class UnicastDestRecvTooFewException(Exception):
    pass

class UnicastDestCaptureNotFoundException(Exception):
    pass

class UnicastDestRecvTooManyException(Exception):
    pass
        
class CaptureSet:
    """Represents one folder holding captures and corresponding json files, as well as one _captureset.json
    
    Parameters
    ----------
    foldername: str
        folder to look for. Absolute or relative path without trailing slash.
    display_filter: str
        display_filter passed to `Capture` objects.
    nocache: bool
        nocache flag passed to `Capture` objects.
    """
    
    def __init__(self, foldername, display_filter='', nocache=False, debug=False, threads=1):
        
        self.foldername = foldername
        self.basename = os.path.basename(foldername)
        self.display_filter = display_filter
        self.nocache = nocache
        self.debug = debug
        self.threads = threads
        
        if debug:
            print_t("CaptureSet: Running in debug mode.")
            self.print_debug = lambda s, *args, **kwargs: print('[%s]'%self.foldername, s, *args, **kwargs)
            self.print_t_debug = lambda s, *args, **kwargs: print_t('[%s]'%self.foldername, s, *args, **kwargs)
        else:
            self.print_debug = lambda s, *args, **kwargs: None
            self.print_t_debug = lambda s, *args, **kwargs: None

        self.print_t_debug("Working on directory", self.foldername)

        # self._json_filenames = subprocess.check_output(
        #         'ls -1 {} | grep -v _captureset | grep -v orig | grep -v nc01 | sort'.format(os.path.join(self.foldername, '*.json')),
        #         shell=True
        #     ).decode('utf8').splitlines()
        self._json_filenames = [f for f in glob(os.path.join(self.foldername, '*.json')) if not any(
                (needle in f for needle in ('orig', '_captureset', UNICAST_DEST_HOSTNAME, '_wlan_scan'))
            )
        ]
        if self._json_filenames == []: raise ValueError('no JSON files found in '+self.foldername)

        # load _captureset.json
        with open(os.path.join(self.foldername, '_captureset.json')) as json_fd:
            self.__dict__.update(json.load(json_fd))

    def parse(self):

        # broadcast flag
        self.broadcast = self.dest_ip.endswith('255')

        # if unicast: check that unicast_dest has received all the packets (which means no queueing losses occurred)
        if not self.broadcast:
            markerfile = os.path.join(self.foldername, '_parse_unicast_dest_pktmismatch')
            if os.path.isfile(markerfile):
                self.print_t_debug('Found _parse_unicast_dest_pktmismatch marker')
                #with open(markerfile) as fd:
                #    self.unicast_dest_count_pkts = int(''.join(fd.readlines()))
                raise UnicastDestRecvTooFewException()
            else:
                if glob(os.path.join(self.foldername, '_parsed_by*')):
                    self.print_t_debug('Omit checking length of unicast_dest because found _parsed_by* marker')
                    pass # we did that already when parsing, now we want to load the captures quickly
                else:
                    self.print_t_debug('Check length of unicast_dest capture')
                    # load the capture from nc01, count it's packets and see if it matches with the numpkt parameter from _captureset.json
                    try:
                        unicast_dest_json_filename, = glob(os.path.join(self.foldername, UNICAST_DEST_HOSTNAME+'*.json'))
                    except ValueError:
                        #raise UnicastDestCaptureNotFoundException()
                        self.print_t_debug('No (or more than one) capture from unicast destination in this captureset')
                    else: 
                        self.print_t_debug("found json file for unicast_dest capture:", unicast_dest_json_filename)
                    
                        # create capture object for unicast_dest
                        c = Capture(unicast_dest_json_filename, display_filter=self.display_filter, nocache=self.nocache, debug=self.debug)
                        # get length (no full parsing needed)
                        self.unicast_dest_count_pkts = c.getlen()
        
                        if self.unicast_dest_count_pkts != self.numpkt:
                            self.print_t_debug('Unicast destination {} should have received {} packets, but got {}!'.format(
                                UNICAST_DEST_HOSTNAME, self.numpkt, self.unicast_dest_count_pkts
                            ))
                            self.print_t_debug("Writing a marker _parse_unicast_dest_pktmismatch to this captureset.")
                            with open(markerfile, 'w') as fd:
                                fd.write(str(self.unicast_dest_count_pkts))
    
                            raise UnicastDestRecvTooFewException()

                        # if self.unicast_dest_count_pkts < self.numpkt:
                        #     raise UnicastDestRecvTooFewException('Unicast destination {} should have received {} packets, but got only {}!'.format(
                        #         UNICAST_DEST_HOSTNAME, self.numpkt, self.unicast_dest_count_pkts
                        #     ))
                        # elif self.unicast_dest_count_pkts > self.numpkt:
                        #     raise UnicastDestRecvTooManyException('Unicast destination {} should have received {} packets, but got {}!'.format(
                        #         UNICAST_DEST_HOSTNAME, self.numpkt, self.unicast_dest_count_pkts
                        #     ))

        "Load and parse one capture, multithreaded version. Called by the threadpool, once per json_filename"
        def getcapture_multithreaded(json_filename, lock, jobnum):

            max_tries = 10
            tries = 0
            with lock:
                self.print_t_debug("[job {:02d}/{:02d}] getcapture_multithreaded got called with arguments json_filename={}, lock={}, jobnum={}".format(
                        jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                        len(self.tfinish),
                        json_filename,
                        lock,
                        jobnum
                    )
                )

            # create capture object
            c = Capture(json_filename, display_filter=self.display_filter, nocache=self.nocache, debug=self.debug)

                        

            # make loop for parsing until successful (sometimes it fails with no obvious reason)
            while True:
                tries += 1
                with lock:
                    self.print_t_debug("[job {:02d}/{:02d}] starting try {}/{}".format(
                            jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                            len(self.tfinish),
                            tries,
                            max_tries
                        )
                    )
                try: 
                    c.walk_pcap()
                    break
                except Exception as e:
                    with lock:
                        print_t('[job {:02d}/{:02d}] c.walk_pcap() raised Exception e:\n'.format(
                                jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                                len(self.tfinish),
                            ),
                            # '          e.__class__: {}\n'.format(e.__class__),
                            # '          type(e): {}\n'.format(type(e)),
                            # '          str(e): {}\n'.format(str(e)),
                            # #'          e.message: {}\n'        # todo check if folder and remove trailing slash.format(e.message),
                            # '          e: ', e
                        )
                        print_t(format_exc()) # print traceback
                    if tries <= max_tries:
                        with lock:
                            print_t("[job {:02d}/{:02d}] Trying again".format(
                                    jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                                    len(self.tfinish),
                                )
                            )
                    else:
                        with lock:
                            print_t('[job {:02d}/{:02d}] Maximum number of tries ({}) reached. Re-raising exception now.'.format(
                                    jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                                    len(self.tfinish),
                                    max_tries
                                )
                            )
                        raise e 

            with lock:
                #finished[0] += 1
                #tfinished.append(datetime.now())
                #print_t("[{:02d}] finished after {} ({} tries): {}".format(
                #        finished[0], tfinished[-1]-self.tstart, tries, c.basename
                #    )
                #)
                self.tfinish[jobnum] = datetime.now()
                self.print_t_debug("[job {:02d}/{:02d}] finished {} after {} ({} tries)".format(
                        jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                        len(self.tfinish),
                        c.basename,
                        self.tfinish[jobnum]-self.tstart,
                        tries
                    )
                )
            c.save_cache() # save the just parsed data immediately (so that it's not lost if this script crashes)
            with lock:
                self.print_t_debug("[job {:02d}/{:02d}] returning now.".format(
                        jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                        len(self.tfinish)
                    )
                )
            return c

        "Load and parse one capture, singlethreaded version"
        def getcapture_singlethreaded(json_filename, jobnum):
            
            max_tries = 10
            tries = 0
            self.print_t_debug("[job {:02d}/{:02d}] loading {}".format(
                    jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                    len(self.tfinish),
                    json_filename
                )
            )

            # create capture object
            c = Capture(json_filename, display_filter=self.display_filter, nocache=self.nocache, debug=self.debug)

            # make loop for parsing until successful (sometimes it fails with no obvious reason)
            while True:
                tries += 1
                self.print_t_debug("[job {:02d}/{:02d}] starting try {}/{}".format(
                        jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                        len(self.tfinish),
                        tries,
                        max_tries
                    )
                )
                try: 
                    c.walk_pcap()
                    break
                except Exception as e:
                    print_t('[job {:02d}/{:02d}] c.walk_pcap() raised Exception:\n'.format(
                            jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                            len(self.tfinish),
                        ),
                        # '          e.__class__: {}\n'.format(e.__class__),
                        # '          type(e): {}\n'.format(type(e)),
                        # '          str(e): {}\n'.format(str(e)),
                        # #'          e.message: {}\n'.format(e.message),
                        # '          e: ', e
                    )
                    print_t(format_exc()) # print traceback

                    if tries <= max_tries:
                        print_t("[job {:02d}/{:02d}] Trying again".format(
                                jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                                len(self.tfinish),
                            )
                        )
                    else:
                        print_t('[job {:02d}/{:02d}] Maximum number of tries ({}) reached. Re-raising exception now.'.format(
                                jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                                len(self.tfinish),
                                max_tries
                            )
                        )
                        raise e 

            #finished[0] += 1
            #tfinished.append(datetime.now())
            #print_t("[{:02d}] finished after {} ({} tries): {}".format(
            #        finished[0], tfinished[-1]-self.tstart, tries, c.basename
            #    )
            #)
            self.tfinish[jobnum] = datetime.now()
            self.print_t_debug("[job {:02d}/{:02d}] finished {} after {} ({} tries)".format(
                    jobnum+1, # s.t. we start at 1/20 and finish at 20/20
                    len(self.tfinish),
                    c.basename,
                    self.tfinish[jobnum]-self.tstart,
                    tries
                )
            )
            c.save_cache() # save the just parsed data immediately (so that it's not lost if this script crashes)
            return c
        
        self.tstart = datetime.now()
        self.print_t_debug("starting to load {} captures".format(len(self._json_filenames)))
        self.tfinish = [None]*len(self._json_filenames) # empty list

        if self.threads > 1:
            "Create a thread pool and call getcapture_multithreaded for every json file"        
            with ThreadPool(self.threads) as pool: # use only 2 threads s.t. tshark processes can use the other 2
                lock = Lock()
                # call getcapture(lock, json_filename) with all elements of the captures list. 
                # starmap(func, [(1,2), (3,4), ...)] calls func(1,2), func(3,4), ... hence the zip
                self.captures = pool.starmap(
                    getcapture_multithreaded,
                    list(
                        zip( # prepare arguments
                            self._json_filenames,
                            [lock]*len(self._json_filenames), # pass lock object (same to all calls)
                            #[self.finished]*len(self._json_filenames), # pass finished counter
                            list(range(len(self._json_filenames))) # pass jobnum i.e. tfinished list index (individual to each call)
                            #[self.tfinish]*len(self._json_filenames), # pass tfinished list (same to all calls) - update: no need to pass a class attribute..
                            #[self.nocache]*len(self._json_filenames), # pass nocache flag (same to all calls) - update: no need to pass a class attribute..
                        )
                    )
                )
            self.print_t_debug("waiting for ThreadPool to quit; calling pool.join()")
            pool.join()

        else:
            self.captures = []
            for jobnum, json_filename in enumerate(self._json_filenames):
                self.captures.append(getcapture_singlethreaded(json_filename, jobnum))

        self.print_t_debug("finished {} captures in {}".format(len(self.captures), self.foldername))
        self.ttotal = max(self.tfinish)-self.tstart
        self.print_t_debug('time: {} (mean: {} per capture)'.format(self.ttotal, self.ttotal/len(self.captures)))
