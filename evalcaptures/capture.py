#!/usr/bin/env python3

import json
import pickle
import os
import dateutil.parser
import numpy as np
# pylint shows an error here, but it seems to work fine..
import mypyshark as pyshark # modified version to support tcpdump -C (wireshark profile selection..)

from datetime import datetime
from util import print_t, sprint_t, Bunch

class Capture:
    """Represents one capture obtained by pycluster.ClusterRun (see ../80211bitflips/pycluster/ClusterRun)

    Parameters
    -----------
    json_filename : str
        JSON file corresponding to the capture, loaded to instance attributes
    display_filter : str
        Tshark display filter to apply
    nocache : bool, optional
        omit cached pickle file and re-parse the capture (default False)

    Examples
    --------
    Parse a capture referenced in mycapture.json
    >>> cap = Capture(self, 'mycapture.json', 'udp'):
    >>> cap.walk_pcap()
    >>> cap.savecache()
    """

    def _log(self, logstr): 
        print_t('[in capture {}] {}'.format(self.basename, logstr))
    
    def __init__(self, json_filename, display_filter='', nocache=False, debug=False):

        self._debug = debug
        if not debug:
            self._log = lambda *args: None # bypass logging
        
        self.json_filename = json_filename
        self.display_filter = display_filter
        
        with open(self.json_filename, encoding='utf-8') as json_fd:
            jsondict = json.load(json_fd)
        
        self.__dict__.update(jsondict) # set as object attributes

        # absolute pathnames with underscore, so they dont get cached
        self._json_abs_path = os.path.abspath(json_filename)
        self._dir_abs_path = os.path.abspath(os.path.dirname(json_filename))
        self._pcap_abs_path = os.path.join(self._dir_abs_path, self.pcap_filename)
        self._path_commonprefix = os.path.commonprefix([self._json_abs_path, self._pcap_abs_path]) # strip extension
        self.basename = os.path.basename(self._path_commonprefix)
        self._pkl_abs_path = self._path_commonprefix+'pkl'
        self._bin_abs_path = self._path_commonprefix+'bin'
                
        # also take parameters from the whole capture set
        # try:
        with open(os.path.join(self._dir_abs_path, '_captureset.json')) as json_fd:
            self.__dict__.update(json.load(json_fd))
        # except FileNotFoundError:
        #     # legacy - to be removed
        #     self._log('legacy fallback, assuming pktdatalen=1000B and numpkt=1024')
        #     self.pktdatalen = 1000
        #     self.numpkt = 1024
        
        # load cache pickle if it exists
        if nocache == False:
            try:
                with open(self._pkl_abs_path, 'rb') as pkl_fd:
                    self._log("opened {} without error ({} bytes)".format(
                            self._pkl_abs_path, os.path.getsize(self._pkl_abs_path)
                        )
                    )
                    pkldict = pickle.load(pkl_fd)
                self.__dict__.update(pkldict) # use saved object attributes from cache
                self._log('using pickle cache file')
            except FileNotFoundError:
                self._log('no cached pkl found')
            except EOFError as e:
                self._log('got EOFError {} while reading pickle'.format(e))
        else:
            self._log('nocache flag given: reparsing')
        
        # set some flags (convenience)
        try: self.pcap_walked
        except AttributeError: self.pcap_walked = False

    def getlen(self):
        """"""
        self._pyshark_obj = pyshark.FileCapture(
            self._pcap_abs_path,
            #display_filter=self.display_filter,
            #configuration_profile='udp_only' # needs Pyshark hack (mypyshark)!
        )
        
        pktcount = 0
        for pkt in self._pyshark_obj:
            pktcount += 1

        del self._pyshark_obj # destroy explicitly

        self._log('calculated length of this capture to %d packets' % pktcount)

        return pktcount

        # doesn't work!
        # return len(self._pyshark_obj)

    def walk_pcap(self):
        """Load and walk through this capture, parsing all packets, and save results to instance attributes.
        
        If cache pickle file is available, load it instead, unless the 'nocache' attribute is set.
        """
        if self.pcap_walked: # we have that cached!
            return
        
        if not os.path.isfile(self._pcap_abs_path): # we dont have cache, but the pcap file doesnt' exist (anymore)
            raise RuntimeError('Could not read '+self._pcap_abs_path)

        self._log('creating pyshark FileCapture object..')       
        #print("[mypyshark2] Opening {} with display_filter='{}' ".format(self._pcap_abs_path,display_filter))
        self._pyshark_obj = pyshark.FileCapture(
            self._pcap_abs_path, 
            display_filter=self.display_filter, 
            configuration_profile='udp_only' # needs Pyshark hack (mypyshark)!
        )

        if self._debug: self._pyshark_obj.set_debug()
        
        ## all attributes starting with count_ or discard_ are counted values over the whole capture.
        self.count_pkts = 0
        self.discard_mcs_missing = 0
        self.discard_mcs_incorrect = 0
        self.discard_datarate_missing = 0
        self.discard_datarate_incorrect = 0
        self.count_retransmissions = 0
        self.discard_macsa_incorrect = 0
        self.count_badfcs = 0
        self.discard_ip_missing = 0
        self.discard_ipdst_incorrect = 0
        self.discard_ipproto_incorrect = 0
        self.discard_udp_missing = 0
        self.discard_udpport_incorrect = 0
        self.discard_data_missing = 0
        self.discard_datalen_invalid = 0
        self.count_netpkts = 0
        self.count_netbytes = 0
        self.count_bitflips = 0
        
        # make an array for self.bitflip_poscounts
        if self.pktdatalen*8 > 0xFFFFFFFF: # uint32 size
            raise RuntimeError('uint32 is too small') # should not happen
        self.bitflip_poscounts = np.zeros(self.pktdatalen*8, dtype='uint32')

        
        # factor of how much more packets are expected to be received than sent because of retransmissions
        # in the worst case (for memory preallocation s.t. loop runs faster - is trimmed to the actual number 
        # of arrived packets later)
        retries_safefactor = 2
        
        # arrays that need one element for each packet (scaling with numpkt)
        self.time_epoch = np.full(int(retries_safefactor*self.numpkt), np.nan, dtype=np.float64)
        self.timedelta = np.full(int(retries_safefactor*self.numpkt), np.nan, dtype=np.float32)
        self.dbm_antsignal = np.full(int(retries_safefactor*self.numpkt), np.nan, dtype=np.float32)
        self.retransmission = np.zeros(int(retries_safefactor*self.numpkt), dtype=np.bool_)
        # number of bitflips per packet, worst case: 1400*8 = 11200 so uint16 is big enough
        self.bitflips = np.zeros(int(retries_safefactor*self.numpkt), dtype=np.uint16)

        #2018-07-05 Quickndirty: Also save the complete packet data
        # self.data = []

        self._rawdata = b''

        "walk through the capture, packet by packet"
        self._log('going through the packets...')
        i=0
        for pkt in self._pyshark_obj:
           
            self.count_pkts += 1
            i = self.count_pkts - 1 # array index separately, for convenience and to avoid bugs!

            framenum = int(pkt.frame_info.number) # also for convenience
            
            "basic metadata: arrival time"
            try:
                self.time_epoch[i] = pkt.frame_info.time_epoch # auto typecast by numpy array
            except AttributeError:
                # do not recover from this
                raise AttributeError('[{:6d}] got AttributeError while reading pkt.frame_info.time_epoch'.format(framenum))
            try:
                self.timedelta[i] = pkt.frame_info.time_delta # auto typecast by numpy array
            except AttributeError:
                # do not recover from this
                raise AttributeError('[{:6d}] got AttributeError while reading pkt.frame_info.time_delta'.format(framenum))

            "save antenna signal strength"
            try:
                self.dbm_antsignal[i] = pkt.radiotap.dbm_antsignal # auto typecast by numpy array
            except AttributeError:
                # do not recover from this
                raise AttributeError('[{:6d}] got AttributeError while reading pkt.radiotap.dbm_antsignal'.format(framenum))

            "check if it has the right MCS (11n) or data rate (11g)"
            if(self.wifistandard == '11n'):
                try:
                    if(int(pkt.radiotap.mcs_index) != self.mcs):
                        self.discard_mcs_incorrect += 1
                        self._log('[{:6d}] has incorrect MCS index ({} instead of {})'.format(framenum, pkt.radiotap.mcs_index, self.mcs))
                        continue
                except AttributeError:
                    self.discard_mcs_missing += 1
                    self._log('[{:6d}] got AttributeError while reading pkt.radiotap.mcs_index - this doesnt belong to our capture'.format(framenum))
                    continue
            elif(self.wifistandard == '11g'):
                try:
                    if(int(pkt.radiotap.datarate)*1000 != self.ratekbps):
                        self.discard_datarate_incorrect += 1
                        self._log('[{:6d}] has incorrect datarate ({} instead of {} kbps)'.format(framenum, (pkt.radiotap.datarate)*1000, self.ratekbps))
                        continue
                except AttributeError:
                    self.discard_datarate_missing += 1
                    self._log('[{:6d}] got AttributeError while reading pkt.radiotap.datarate - this doesnt belong to our capture'.format(framenum))
                    continue
            
            "Is this a MAC layer retransmission?"
            # is that a retransmission? (so we can exclude this packet from arrival time calculations later)
            # # pkt.wlan.fc is a string e.g. '0x8802', not further decoded. We need bit 3 from 2nd octet
            # # evaluates to True if not null (the array is dtype=np.bool_)
            # try:
            #     self.retransmission[i] = (int(pkt.wlan.fc, 16) & 0x0008) != 0
            # except AttributeError:
            #     # do not recover from this
            #     raise AttributeError('[{:6d}] got AttributeError while reading pkt.wlan.fc')
            # [update] found it now -.-
            try:
                if pkt.wlan.fc_retry == '1':
                    self.retransmission[i] = True
                    self.count_retransmissions += 1
                    self._log("[{:6d}] is a retransmitted packet".format(framenum))
                    continue
            except AttributeError:
                # do not recover from this
                raise AttributeError('[{:6d}] got AttributeError while reading pkt.wlan.fc_retry'.format(framenum))

            
            "check IEEE802.11 (DL/MAC) layer"
            # Check source address. Should be usb nic 00:0f:c9:14:eb:44 (interface enx000fc914eb44 on nuc)
            # Hardcoded here by purpose.
            try:
                if pkt.wlan.sa != '00:0f:c9:14:eb:44':
                    self.discard_macsa_incorrect += 1
                    self._log("[{:6d}] source mac adress incorrect".format(framenum))
                    continue
            except AttributeError:
                # do not recover from this
                raise AttributeError('[{:6d}] got AttributeError while reading pkt.wlan.sa'.format(framenum))
            
            "check IP layer"
            try: pkt.ip
            except AttributeError:
                self.discard_ip_missing += 1
                self._log("[{:6d}] IP layer missing".format(framenum))
                continue
            "check destination IP"
            try:
                if pkt.ip.dst != self.dest_ip:
                    self.discard_ipdst_incorrect += 1
                    self._log("[{:6d}] destination ip address is incorrect: {}".format(framenum, pkt.ip.dst))
                    continue
            except AttributeError:
                self.discard_ip_missing += 1
                self._log("[{:6d}] destination ip address not readable".format(framenum))
                continue
            "check protocol"
            try:
                if int(pkt.ip.proto) != 17:
                    self.discard_ipproto_incorrect += 1
                    self._log("[{:6d}] ip.proto is not 17 (udp): {}".format(framenum, pkt.ip.proto))
                    continue
            except AttributeError:
                self.discard_ip_missing += 1
                self._log("[{:6d}] ip.proto is not readable".format(framenum))
                continue
            
            "check UDP layer"
            try: pkt.udp
            except AttributeError:
                self.discard_udp_missing += 1
                self._log("[{:6d}] UDP layer missing".format(framenum))
                continue
            try:
                if int(pkt.udp.dstport) != self.dest_port:
                    self.discard_udpport_incorrect += 1
                    self._log("[{:6d}] udp.dstport is incorrect: {}".format(framenum, pkt.udp.dstport))
                    continue
            except AttributeError:
                self.discard_udp_missing += 1
                self._log("[{:6d}] udp.dstport is not readable".format(framenum))
                continue
            
            "check 'data' layer"
            try: pkt.data
            except AttributeError:
                self.discard_data_missing += 1
                self._log("[{:6d}] Data layer missing".format(framenum))
                continue
            # check length of data field
            try:
                if (int(pkt.data.len)*8) != len(self.bitflip_poscounts):
                    self.discard_datalen_invalid +=1
                    self._log("[{: 6d}] pkt.data.len*8={} bits doesnt match with expected {} bits".format(
                        framenum, int(pkt.data.len)*8, len(self.bitflip_poscounts)))
                    continue
            except AttributeError:
                self.discard_datalen_invalid += 1
                self._log("[{: 6d}] pkt.data.len not readable (catched AttributeError)".format(framenum))
                continue
                # # do not recover from this
                # raise AttributeError('[{:6d}] got AttributeError while reading pkt.data.len'.format(framenum))
                
            "if we made it up to here, the data is undamaged enough to possibly be useful!"
            self.count_netbytes += int(pkt.data.len)
            self.count_netpkts += 1

            "Now check if it's a partial packet. Frame check sequence failed?"
            try:
                #if pkt.wlan.fcs_status != '1':
                if pkt.radiotap.flags_badfcs == '1':
                    self.count_badfcs += 1
                    self._log("[{:6d}] FCS bad".format(framenum))
                    # go on!
            except AttributeError:
                # do not recover from this
                raise AttributeError('[{:6d}] got AttributeError while reading pkt.radiotap.flags_badfcs'.format(framenum))
            

            try:
                if self.randomdata: randomdata = True
                else: randomdata = False
            except AttributeError: # this flag hasnt been there forever - fallback
                randomdata = False

            
            if not self.randomdata:

                """go through the packet bit by bit and detect `1` bits ("bitflip"). When one found, increment the 
                numpy array self.bitflip_poscounts at the corresponding position. Also set self.bitflips[i] with 
                the number of occurred bitflips in this packet, where i is the current packet number.
                """
                #make numpy array from whole data field and use np.unpackbits
                # ("unpackbits_mv" method)
                try: 
                    bitflip_positions = np.unpackbits(
                        np.array(
                            memoryview(pkt.data.data.binary_value), # makes bytes iterable
                            np.uint8 # force uint8 for the array
                        ), 
                    )
                    self.bitflip_poscounts += bitflip_positions # element-wise addition of bits
                    if any(bitflip_positions):
                        self._log("[{:6d}] {} bitflips detected".format(framenum, sum(bitflip_positions)))
                    self.bitflips[i] = sum(bitflip_positions) # sum of `True`s is number of bitflips

                    #2018-07-05 Quickndirty: Also save the complete packet data
                    #self.data.append(np.array(memoryview(pkt.data.data.binary_value), np.uint8))

                    #2018-07-28: Better save outside of pkl
                    self._rawdata += pkt.data.data.binary_value

                except Exception as e:
                    self._log("[{:6d}] caught exception while counting bitflips: {}".format(
                        framenum, e))
                    raise e

            
            else:
                #TODO compare with arrived data at unicast_dest..
                pass
        
        "Sum bitflips to obtain total number of bitflips in this capture"
        self.count_bitflips = self.bitflip_poscounts.sum()
        # check that number with summing self.bitflips over all packets, must be the same!
        if self.count_bitflips != self.bitflips.sum():
            raise RuntimeError(
                'Two calculation methods do not return the same value: self.bitflip_poscounts.sum() = {} but self.bitflips.sum() = {} !'.format(
                    self.bitflip_poscounts.sum(), self.bitflips.sum()
                )
            )
                
        "trim arrays to the actual number of packets that have been parsed"
        self.time_epoch = self.time_epoch[0:i] # indices from 0 to count_pkts-1
        self.timedelta = self.timedelta[0:i]
        self.dbm_antsignal = self.dbm_antsignal[0:i]
        self.retransmission = self.retransmission[0:i]
        self.bitflips = self.bitflips[0:i]
        
        "Save raw data to a .bin file for further processing"
        self.bin_filename = self.pcap_filename + '.bin'
        with open(self._bin_abs_path, 'wb') as fd:
            fd.write(self._rawdata)
        
        self._log("finished calculating: {} packets in capture, {} possibly useful".format(
            self.count_pkts, self.count_netpkts))
        
        "set this flag to indicate cached data"
        self.pcap_walked = True
        
        "cleanup pyshark object as it's no more needed"
        self._pyshark_obj.close() # close all connections from pyshark to tshark etc. - hangs without this line!
        del self._pyshark_obj # delete FileCapture object

        # # UPDATE: dont' do that, its better when they all have the same length (e.g. for matrix generation with np.vstack)        
        # # trim arrays with one element for each packet, s.t. they are not full of nan at the end when packets are lost
        # #self.timedelta = self.timedelta[~numpy.isnan(self.timedelta)]
        # #self.retransmission = self.retransmission[~numpy.isnan(self.retransmission)]
        # self.timedelta = self.timedelta[:self.pktcount]
        # self.retransmission = self.retransmission[:self.pktcount]

    "save all non-underscore object attributes to a pickle file"
    def save_cache(self):
        if self.pcap_walked: # only when there is data cached
            # dont save absolute paths and non-serializable attributes (prefixed with '_')
            save_dict = {key: val for key, val in self.__dict__.items() if not key.startswith('_')}
            with open(self._pkl_abs_path, 'wb') as fd:
                pickle.dump(save_dict, fd)
