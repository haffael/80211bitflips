try:
    LISTEN_CHANNEL = sys.argv[1]
    #LISTEN_SECONDS = sys.argv[2]
except KeyError:
    #print("usage: "+sys.argv[0]+" <wifi_ch> <seconds>")
    print("usage: "+sys.argv[0]+" <wifi_ch>")

ssh_userhost_tuples = [
    ('root','zbox-kali'),
]

from paramiko.client import SSHClient
#from paramiko.channel import Channel

#try: client.close() # close previous connection if it still exists
#except NameError: pass

# wrapper for executing a command - raise exception when exit code is not 0
class SSHQueryHasErr(Exception): pass
from time import time # debug - timestamps
def query(client, cmd, *args, **kwargs):
    print('[%.6f] running exec_command()...' % time())
    stdin, stdout, stderr = client.exec_command(cmd, *args, **kwargs)
    print('[%.6f] running stderr.readlines()...' % time())
    err = ''.join(stderr.readlines()).rstrip() # join lines and remove \n at end - blocks until cmd returns!
    if(err): 
        print('[%.6f] stderr is not empty - raising exception' % time())
        raise SSHQueryHasErr(err)
    print('[%.6f] running stdout.readlines()...' % time())
    return ''.join(stdout.readlines()) # not efficient - better return stdout directly?
SSHClient.query = query # bind as method

client = SSHClient()
client.load_system_host_keys()
for ssh_userhost_tuple in ssh_userhost_tuples:
    client.connect(ssh_host, username=ssh_user) # no password (public key auth)
    try: 
        client.query('mon0ch %d' % LISTEN_CHANNEL)
        client.query('screen mon0capture')
        client.close()
    except SSHQueryHasErr as err:
        print("ERROR on {host}: \n {err}".format(host=host, err=err))
