#!/usr/bin/env python3

import numpy as np

def test_bitflip_poscount_withint():
    testpkgs_int = np.array([
        0b01010101010101010101010101010101,
        0b10101010101010101010101010101010,
        0b01010101010101010101010101010101,
        0b10101010101010101010101010101010
    ])
    plen_bits = 32
    expected_result = np.ones(plen_bits)*2

    print("testing {} packages with {} bit each".format(len(testpkgs_int), plen_bits))
    assert np.array_equal(bitflip_poscount_withint(testpkgs_int,plen_bits), expected_result)

def bitflip_poscount_withint(pkgs_int,plen_bits):

    # make string and strip "0b:
    pkgs_str = [ bin(pkgdata)[2:].rjust(plen_bits,'0') for pkgdata in pkgs_int ]

    # initialize result array with zeros
    result = np.zeros(plen_bits, dtype='uint32')

    for bitpos in range(0,plen_bits): # go over bit positions
        for pkg_str in pkgs_str: # go over packets
            result[bitpos] += 1 if pkg_str[bitpos] == '1' else 0
    return result


def test_bitflip_poscount_withbytes():
    testpkgs = [
        b'\xAA\xAA', # 1010...
        b'\x55\x55', # 0101...
        b'\xAA\xAA', # 1010...
        b'\x55\x55' # 0101...
    ]
    plen_bits = 16
    expected_result = np.array([2]*16) # every bit is set two times

    print("testing {} packages with {} bit each".format(len(testpkgs), plen_bits))
    assert np.array_equal(bitflip_poscount_withbytes(testpkgs), expected_result)

def bitflip_poscount_withbytes(pkgs_bytes):
    # make multidimensional numpy array of bytes
    pkgs_uint8 = np.array([list(p) for p in pkgs_bytes], np.uint8)
    pkgs_bits = np.unpackbits(pkgs_uint8, axis=1)
    return np.sum(pkgs_bits, axis=0)
    