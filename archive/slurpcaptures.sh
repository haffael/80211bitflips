#!/usr/bin/env bash
[ `whoami` != 'nuc' ] && { echo "only run this on nuc!"; exit 1; }
[ `pwd` != '/home/nuc/bigdata/raphael' ] && { echo "only run in ~/bigdata/raphael/"; exit 1; }
if [ $# -ne 1 ]; then
       	cat <<-USAGE
	Use parallel-slurp to get captures from hosts specified in <hostsfile>
	Usage: $0 <hostsfile>
	USAGE
	exit 1
fi
if ! [ -f "$1" ]; then
	echo "Hosts file not found: $1"
	exit 1
fi
cmd="parallel-slurp -h '$1' -v -L captures -e errors/ '/home/ubuntu/raphael/*.{json,pcap}' ."
echo "> $cmd"
eval $cmd
[ $? -ne 0 ] && { echo "parallel-slurp failed."; exit 1; }
mv captures/*/* captures/ || { echo "Nothing transmitted"; exit 1; }
rm -r captures/192*
echo "Check the data and remove the original files on the nodes."
